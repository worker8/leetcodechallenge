class Solution {
    fun letterCasePermutation(input: String): List<String> {
        val store = mutableListOf<String>()

        for (i in 0 until input.length) {
            val char = input[i]
            println("char: $char")

            if (char.isLetter()) {
                if (store.isEmpty()) {
                    store.add(char.toLowerCase().toString())
                    store.add(char.toUpperCase().toString())
                } else {
                    for (j in 0 until store.size) {
                        val curr = store[j]
                        store[j] = curr + char.toLowerCase()
                        store.add(curr + char.toUpperCase())
                    }
                }

            } else if (char.isDigit()) {
                if (store.isEmpty()) {
                    store.add(char.toString())
                } else {
                    for (j in 0 until store.size) {
                        store[j] = store[j] + char
                    }
                }
            }
            println("store: $store")
        }
        return store
    }
}

class Solution3 {
    fun letterCasePermutation(input: String): List<String> {
        inputString = input
        solve(mutableListOf(input), 0)
        return store.toList()
    }

    var inputString = ""
    val store = mutableListOf<String>()
    fun solve(list: MutableList<String>, position: Int) {
        println("list: $list, position: $position")
        if (position == inputString.length) {
            store.addAll(list)
            return
        } else {
            for (i in 0 until list.size) {
                val item = list[i]
                if (item[position].isLetter()) {
                    val newChar = if (item[position].isUpperCase()) {
                        item[position].toLowerCase()
                    } else {
                        item[position].toUpperCase()
                    }
                    solve(mutableListOf(item, item.replaceCharAt(newChar, position)), position + 1)
                } else {
                    solve(mutableListOf(item), position + 1)
                }
            }
        }
    }
}

fun main() {
    val testString = "a1b2"
    //val result = Solution().letterCasePermutation(testString)
    //val result2 = Solution2().letterCasePermutation(testString)
    val result3 = Solution3().letterCasePermutation(testString)
    println("result3: $result3")

//    val hey = "hello world"
//    val charAt = 5
//    println("test: ${hey.replaceCharAt('+', 5)}")
}

fun String.replaceCharAt(x: Char, position: Int): String {
    return substring(0, position) + x + substring(position + 1, length)
}

