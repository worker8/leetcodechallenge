

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class Solution2 {
    public List<String> letterCasePermutation(String S) {
        if (S == null) {
            return new LinkedList<String>();
        }
        Queue<String> queue = new LinkedList<>();
        queue.offer(S);
        System.out.println("queue1: " + queue);
        for (int i = 0; i < S.length(); i++) {
            System.out.println("queue2: " + queue);
            if (Character.isDigit(S.charAt(i))) continue;
            int size = queue.size();
            for (int j = 0; j < size; j++) {
                String cur = queue.poll();
                System.out.println("cur: " + cur);
                char[] chs = cur.toCharArray();

                chs[i] = Character.toUpperCase(chs[i]);
                queue.offer(String.valueOf(chs));
                System.out.println("queue3a: " + queue);
                chs[i] = Character.toLowerCase(chs[i]);
                queue.offer(String.valueOf(chs));
                System.out.println("queue3b: " + queue);
            }
            System.out.println("-------");
        }

        return new LinkedList<String>(queue);
    }

    public static void main() {
        Solution2 solution2 = new Solution2();
        List result = solution2.letterCasePermutation("a1b2");

        System.out.println("result: " + result);
    }
}


