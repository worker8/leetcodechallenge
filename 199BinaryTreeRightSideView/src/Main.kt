import java.util.*

class TreeNode(var `val`: Int) {
    var left: TreeNode? = null
    var right: TreeNode? = null
}

class Solution {
    lateinit var store: TreeMap<Int, Int> // level(key) -> `val`(`val`)

    fun rightSideView(root: TreeNode?): List<Int> {
        store = TreeMap()
        traverse(root, 0)
        val result = mutableListOf<Int>()
        for (i in 0 until store.size) {
            store[i]?.let {
                result.add(it)
            }
        }

        return result.toList()
    }


    fun traverse(root: TreeNode?, level: Int) {
        root?.let { _root ->
            //print("${it.`val`}, ")
            if (!store.contains(level)) {
                store[level] = _root.`val`
            }
            traverse(_root.right, level + 1)
            traverse(_root.left, level + 1)
        }
    }
}


// Given a binary tree,
// imagine yourself standing on the right side of it,
// return the values of the nodes you can see ordered from top to bottom.
fun main() {
    val level0 = TreeNode(1)
    val level1a = TreeNode(2)
    val level1b = TreeNode(3)

    val level2a = TreeNode(4)
    val level2b = TreeNode(5)
    val level2c = TreeNode(6)
    val level2d = TreeNode(7)

    val level3a = TreeNode(8)
    val level3b = TreeNode(9)

    level0.left = level1a
    level0.right = level1b

    level1a.left = level2a
    level1a.right = level2b

    level1b.left = level2c
    level1b.right = level2d

    level2a.left = level3a
    level2b.right = level3b

    val solution = Solution()
    val result = solution.rightSideView(level0)

    println("final result: ${result}")
}