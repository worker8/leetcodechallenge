package com.morshe;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Solution solution = new Solution();
        String begin = "hit";
        String end = "cog";

        List<String> wordList = new ArrayList<>();
        wordList.addAll(Arrays.asList("hot", "dot", "dog", "lot", "log", "cog"));
        //Arrays.asList("hot", "dot", "dog", "lot", "log", "cog");
        int ans = solution.ladderLength(begin, end, wordList);
        System.out.println("answer: " + ans);
        char a = 'g';
        char b = 'g';
        System.out.println("diff: " + (a == b));
        System.out.println("wordList before: " + wordList);
        String temp = "hot";

        wordList.remove(temp);
        System.out.println("wordList after: " + wordList);

        Queue<String> queue = new LinkedList<>();
        queue.offer("hot");
        queue.offer("cold");

        System.out.println("poll: " + queue.poll());
        System.out.println("poll: " + queue.poll());
        System.out.println("queue.size: " + queue.size());
        boolean [] test = new boolean[1];
        System.out.println("test: " + test[0]);

        String word = "abcd";
        System.out.println("substring: " + word.substring(1));
    }
}

class TreeNode {
    String val;
    List<TreeNode> nodes;

    TreeNode(String val) {
        this.val = val;
    }
}

class Solution {
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        return -1;
    }

    TreeNode f(String word, String end, List<String> wordList) {
        TreeNode newNode = new TreeNode(word);
        return newNode;

    }
}
