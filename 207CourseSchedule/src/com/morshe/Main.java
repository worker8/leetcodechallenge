package com.morshe;

import javafx.util.Pair;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        int length = 5;
        int[][] testInput = new int[length][2];
        testInput[0] = new int[]{1, 0};
        testInput[1] = new int[]{1, 2};
        testInput[2] = new int[]{2, 3};
        testInput[3] = new int[]{3, 4};
        testInput[length - 1] = new int[]{3, 5};

        int length2 = 2;
        int[][] testInput2 = new int[length2][2];
        testInput2[0] = new int[]{1, 0};
        testInput2[length2 - 1] = new int[]{2, 0};

        Solution solution = new Solution();

        boolean answer = solution.canFinish(3, testInput2);
        System.out.println("answer: " + answer);
    }
}

class Node {
    boolean visited;
    List<Integer> keyList;

    Node(List<Integer> list) {
        this.visited = false;
        this.keyList = list;
    }

    @Override
    public String toString() {
        return "Node(visited: " + visited + ", keyList: " + keyList + ")";
    }
}

class Solution {
    Map<Integer, Node> nodeMap = new HashMap<>(); // store various nodes
    Stack<Integer> stack = new Stack<>();
    Set<Integer> list = new HashSet<>();
    boolean terminate = false;
    int numCourses;
    public boolean canFinish(int numCourses, int[][] p) {
        this.numCourses = numCourses;
        parseIntoMap(p);
        f();

        System.out.println("map: " + nodeMap);
        System.out.println("list: " + list);

        return !terminate && list.size() <= numCourses;
    }

    void f() {
        for (Map.Entry<Integer, Node> entry : nodeMap.entrySet()) {
            doRow(entry.getKey(), entry.getValue());
            if (terminate) {
                break;
            }
        }
    }

    // 2, [[1,0],[1,2],[2,3],[3,4],[3,5]]
    // nodeMap
    // 1 -> [0,2]
    // 2 -> [3]
    // 3 -> [4,5]
    void doRow(Integer key, Node node) {
        if (terminate) {
            return;
        }
        if (stack.contains(key)) {
            terminate = true; // cyclic graph detected
            return;
        } else {
            if (!node.visited) {
                stack.add(key);
                node.visited = true;
                for (Integer pKey : node.keyList) {
                    if (nodeMap.containsKey(pKey)) {
                        doRow(pKey, nodeMap.get(pKey));
                    } else {
                        list.add(pKey);
                        if (list.size()>numCourses){
                            terminate = true;
                        }
                    }
                    if (terminate) {
                        break;
                    }
                }
                if (terminate) {
                    return;
                }
                list.add(stack.pop());
                if (list.size()>numCourses){
                    terminate = true;
                }
            }
        }
    }

    void parseIntoMap(int[][] p) {
        for (int i = 0; i < p.length; i++) {
            int[] row = p[i];
            if (nodeMap.containsKey(row[0])) {
                Node node = nodeMap.get(row[0]);
                node.keyList.add(row[1]);
            } else {
                Node node = new Node(new ArrayList<>());
                node.keyList.add(row[1]);
                nodeMap.put(row[0], node);
            }
        }
    }
}

class Solution2 {
    Stack<Integer> stack = new Stack<>();
    Set<Integer> array = new HashSet<>();
    Map<Integer, Node> map = new HashMap<>();
    int numCourses;
    boolean terminate = false;

    // 2, [[1,0],[1,2],[2,3]]
    public boolean canFinish(int numCourses, int[][] p) {

        // I'm going to use topological sort
        // that is: pushing into a stack as a traverse, then pop back out into an ArrayList

        this.numCourses = numCourses;
        // clear things first, so that it can be re-used
        stack.clear();
        array.clear();
        map.clear();

        intoMap(p);
//        System.out.println("p: " + p[1][0]);
//        System.out.println("map: " + map);
        traverse(map);
        System.out.println("array: " + array);
        if (terminate) {
            System.out.println("terminated");
            return false;
        }
        return numCourses >= array.size();
    }

    private void intoMap(int[][] p) {
        for (int i = 0; i < p.length; i++) {
            int[] row = p[i];
            Node found = map.get(row[0]);
            if (found != null) {
                found.keyList.add(row[1]);
            } else {
                List<Integer> newList = new ArrayList<>();
                map.put(row[0], new Node(newList));
                newList.add(row[1]);
            }
            Node found2 = map.get(row[1]);
            if (found2 == null) {
                map.put(row[1], new Node(new ArrayList<>()));
            }
        }
    }

    private void traverse(Map<Integer, Node> localMap) {
        for (Map.Entry<Integer, Node> entry : localMap.entrySet()) {
            if (terminate) {
                break;
            }
            f(entry.getKey(), entry.getValue());
        }
    }

    void f(Integer key, Node node) {
        if (node == null || node.visited || terminate) {
            return;
        }
        node.visited = true;
        stack.add(key);
        System.out.println("[termination] stack: " + stack);
        System.out.println("[termination] array: " + array);
        if (stack.size() + array.size() > numCourses) {
            terminate = true;
        }
        for (Integer item : node.keyList) {
            if (terminate) {
                break;
            }
            if (map.containsKey(item) && !map.get(item).keyList.isEmpty()) {
                f(item, map.get(item));
            } else if (map.containsKey(item) && map.get(item).keyList.isEmpty()) {
                array.add(item);
            }
        }
        array.add(stack.pop());
    }
}

/**
 * // 2, [[1,0],[1,2],[2,3],[3,4],[3,5]]
 * Map
 * 1 --> 0, 2
 * 2 --> 3
 * 3 --> 4, 5
 * Topo sort
 * ----1    // stack = [1]     ,
 * ---/ \
 * --2  0   // stack = [1,2]   , backtrackArray = [3,2] -----> stack [1,0] -----> backtrackArray  = [3,2,0,1]
 * -/
 * -3       // stack = [1,2,3] , backtrackArray = [3]
 * / \
 * 4 5
 */