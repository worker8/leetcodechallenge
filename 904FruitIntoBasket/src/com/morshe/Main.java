package com.morshe;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] trees = {1, 0, 1, 4, 1, 4, 1, 2, 3};
        int answer = solution.totalFruit(trees);
        System.out.println("answer: " + answer);

    }
}

class Solution {

    int totalFruit(int[] tree) {
        if (tree.length <= 2) {
            return tree.length;
        }

        int pointer1 = 0;
        int pointer2 = 1;
        int num1;
        int num2;
        int max = 0;
        // find the initial window
        while (pointer2 < tree.length && tree[pointer2] == tree[pointer1]) {
            pointer2++;
        }
        if (pointer2 == tree.length) {
            return tree.length;
        }
        max = Math.max(max, pointer2 - pointer1 + 1);

        // num1 & num2 are used to record the last 2 numbers
        // you might wonder why don't we just use tree[pointer1] & tree[pointer2] directly
        // because in this case:
        //  [1,0,1,99]
        //   ^   ^
        // at this point, the window will pointer to 2 ones, so we need num1 & num2 to record 1 and 0
        num1 = tree[pointer1];
        num2 = tree[pointer2];

        // curr points to the next number to check
        int curr = pointer2 + 1;

        while (curr < tree.length) {
            if (tree[curr] != num1 && tree[curr] != num2) { // when it is a new number, we 'slide' the window
                num1 = tree[curr - 1]; // we also need to update the num
                num2 = tree[curr];

                // this is for sliding pointer1 up until where pointer2 is
                int temp = tree[pointer2];
                while (temp == tree[pointer2]) {
                    pointer2--;
                }
                pointer1 = pointer2 + 1;
            }
            pointer2 = curr;
            max = Math.max(max, pointer2 - pointer1 + 1);
            System.out.println(pointer1 + "," + pointer2 + ", max: " + max);
            curr++;
        }


        return max;
    }
}
