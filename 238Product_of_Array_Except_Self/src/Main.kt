class Solution {
    fun productExceptSelf(nums: IntArray): IntArray {
        for (i in 0 until nums.size) {
            println("num: $i")
        }
        return intArrayOf(0, 0, 1, 999)
    }
}

fun main() {
    val input = intArrayOf(1, 2, 3, 4)
    val output = intArrayOf(24, 12, 8, 6)

    val solution = Solution()
    val result = solution.productExceptSelf(input)

    println("result: ${result.toList()}")
}