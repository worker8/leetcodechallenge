package com.morshe;

import java.util.*;

public class Main {

    public static void main(String[] args) {
//        int length = 5;
//        int[][] testInput = new int[length][2];
//        testInput[0] = new int[]{1, 0};
//        testInput[1] = new int[]{1, 2};
//        testInput[2] = new int[]{2, 3};
//        testInput[3] = new int[]{3, 4};
//        testInput[length - 1] = new int[]{3, 0};

        int length2 = 2;
        int[][] testInput2 = new int[length2][2];
        testInput2[0] = new int[]{1, 0};
        testInput2[1] = new int[]{0, 1};

        Solution solution = new Solution();

        int[] result = solution.findOrder(length2, testInput2);

        String temp = "";
        for (int i = 0; i < result.length; i++) {
            int item = result[i];
            temp += item + ", ";
        }
        System.out.println("[" + temp + "]");
    }
}

class Node {
    boolean visited;
    List<Integer> keyList;

    Node() {
        this.visited = false;
        this.keyList = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Node(visited: " + visited + ", keyList: " + keyList + ")";
    }
}

class Solution {
    Map<Integer, Node> map = new HashMap<>();
    Stack<Integer> stack = new Stack<>();
    Set<Integer> set = new LinkedHashSet<>();

    int numCourses;
    boolean terminate;

    Solution() {
        terminate = false;
    }

    public int[] findOrder(int numCourses, int[][] p) {
        this.numCourses = numCourses;

        intoMap(p);
        for (Map.Entry<Integer, Node> entry : map.entrySet()) {
            f(entry.getKey(), entry.getValue());
        }
        if (terminate) {
            return new int[0];
        }
        int[] result = new int[numCourses];
        int i = 0;
        for (Integer item : set) {
            result[i] = item;
            i++;
        }

        return result;
    }


    void f(int key, Node node) {
        if (stack.contains(key)) {
            terminate = true;
        }
        if (node.visited || terminate) {
            return;
        }
        node.visited = true;

        stack.add(key);

        if (!node.keyList.isEmpty()) {
            for (Integer pKey : node.keyList) {
                f(pKey, map.get(pKey));
                if (terminate) {
                    break;
                }
            }
        }
        set.add(stack.pop());
    }

    void intoMap(int[][] p) {
        for (int i = 0; i < numCourses; i++) {
            map.put(i, new Node());
        }

//        for (Map.Entry<Integer, Node> entry : map.entrySet()) {
//            System.out.println("map[" + entry.getKey() + ": " + entry.getValue());
//        }

        for (int i = 0; i < p.length; i++) {
            int[] row = p[i];
            Node node = map.get(row[0]);
            node.keyList.add(row[1]);
        }
    }
}