package com.morshe;

import java.util.*;

public class Main {
    public static void main(String[] args) {
//        SolutionSlow solution = new SolutionSlow();
        Solution solution = new Solution();
        int[] nums = {-2, 1, 2, -2, 1, 2};
        boolean answer = solution.find132pattern(nums);
        System.out.println("answer: " + answer);
    }
}

class SolutionSlow {
    int min;
    List<Range> ranges = new ArrayList<>();

    public boolean find132pattern(int[] nums) {
        if (nums.length < 3) {
            return false;
        }
        min = nums[0];
        boolean found = false;
        for (int i = 1; i < nums.length; i++) {
            int b = nums[i];

            if (!ranges.isEmpty()) {
                for (Range range : ranges) {
                    if (range.lower < b && b < range.higher) {
                        System.out.println("ranges: " + ranges);
                        return true;
                    }
                }
            }

            if (min < b) {
                ranges.add(new Range(min, b));
            } else if (min > b) {
                min = b;
            }
        }
        return found;
    }
}

class Range implements Comparable {
    int lower, higher;

    Range(int lower, int higher) {
        this.lower = lower;
        this.higher = higher;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Range range = (Range) o;
        return lower == range.lower &&
                higher == range.higher;
    }

    @Override
    public int hashCode() {
        return Objects.hash(lower, higher);
    }

    @Override
    public String toString() {
        return "(" + lower + ", " + higher + ")";
    }

    @Override
    public int compareTo(Object o) {
        if (o == this) return 0;
        if (!(o instanceof Range)) {
            return -1;
        }
        Range other = (Range) o;
        if (this.lower == other.lower) {
            return Integer.compare(this.higher, other.higher);
        } else {
            return Integer.compare(this.lower, other.lower);
        }
    }
}

class Solution {
    int min;
    List<Range> ranges = new ArrayList<>();

    public boolean find132pattern(int[] nums) {
        if (nums.length < 3) {
            return false;
        }
        min = nums[0];
        boolean found = false;
        for (int i = 1; i < nums.length; i++) {
            int b = nums[i];

            if (!ranges.isEmpty()) {
                if (binarySearch(b, ranges)) {
                    return true;
                }
//                for (Range range : ranges) {
//                    if (range.lower < b && b < range.higher) {
//                        return true;
//                    }
//                }
            }

            if (min < b) {
                ranges.add(new Range(min, b));
            } else if (min > b) {
                min = b;
            }
        }
        System.out.println("ranges: " + ranges);
        return found;
    }

    private boolean binarySearch(int target, List<Range> list) {
        int start = 0;
        int end = list.size() - 1; // 8
        int mid = (end - start) / 2;

        while (start <= end) {
            Range range = list.get(mid);
            //System.out.println("(start, end, mid) = (" + start + "," + end + "," + mid + "); range = " + range + "; target = " + target);
            if (range.lower < target) {
                if (target < range.higher) {
                    return true;
                } else if (target >= range.higher) {
                    start = mid;
                }
            } else if (target <= range.lower) { // 0, 1
                end = mid;
            } else if (target > range.higher) {
                start = mid;
            }
            mid = (end - start) / 2; //0
            if (end - start <= 1) {
                break;
            }
        }
        return false;
    }
}


class Solution2 {
    public boolean find132pattern(int[] nums) {
        if (nums.length < 3) {
            return false;
        }
        Integer validLow = null, validHigh = null;
        Integer tempLow = nums[nums.length - 1];
        for (int i = nums.length - 2; i >= 0; i--) {
            int num = nums[i];
            if (validLow != null && validHigh != null) {
                if (num < validLow) {
                    return true;
                }
            }

            if (num > tempLow && (validLow == null || num > validLow)) {
                validLow = tempLow;
                validHigh = num;
            } else {
                tempLow = num;
            }

            System.out.println("num, tempLow: " + num + ", " + tempLow);
        }
        return false;
    }
}
