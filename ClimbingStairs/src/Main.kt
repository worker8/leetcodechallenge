import kotlin.test.assertEquals

class Solution1 {
    var hashMap = hashMapOf<Int, Int>()
    fun climbStairs(m: Int): Int {
        hashMap = hashMapOf<Int, Int>()
        return _climb(0, m)
    }

    private fun _climb(n: Int, m: Int): Int {
        if (n == m) {
            hashMap[n] = 1
            return 1
        } else if (n == m + 1) {
            hashMap[n] = 0
            return 0
        } else {
            if (hashMap.containsKey(n)) {
                return hashMap.get(n)!!
            } else {
                val answer = _climb(n + 1, m) + _climb(n + 2, m)
                hashMap[n] = answer
                return answer
            }
        }
    }
}

// f(2) = 2
// f(3) = 3
// f(4) = 5
// f(5) = 8
// f(6) = 13
// f(7) = 21
// f(8) = 34
// f(9) = 55
// f(10) = 89

val testPairList = listOf<Pair<Int, Int>>(
    2 to 2,
    3 to 3,
    4 to 5
)

val testList = listOf(2, 3, 4, 5, 6, 7, 8, 9, 10)

fun main() {
    val solution1 = Solution1()

//    testPairList.forEachIndexed { index, (input, expectedOutput) ->
//        val actual = solution1.climbStairs(input)
//        assertEquals(actual = actual, expected = expectedOutput)
//    }

    testList.forEach {
        val actual = solution1.climbStairs(it)
        println("f($it) = $actual")
    }
}

