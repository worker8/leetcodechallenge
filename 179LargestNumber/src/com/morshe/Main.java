package com.morshe;

import javafx.util.Pair;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

public class Main {

    public static void main(String[] args) {
        int[] input = {121, 12, 9};
        Solution solution = new Solution();
        String answer = solution.largestNumber(input);
        System.out.println("answer: " + answer);
        String temp = "12342312098765789";
        temp.substring(0);
        Pair<Integer, Integer> pair = new Pair<>(1,2);

        Character c = temp.charAt(0);

    }
}

/**
 * has weird compare mechanism
 */
class IntegerX implements Comparable {
    Integer x;

    IntegerX(Integer x) {
        this.x = x;
    }

    @Override
    public int compareTo(Object o) {
        if (o == this) {
            return 0;
        }
        if (!(o instanceof IntegerX)) {
            return -1;
        }
        IntegerX otherIntegerX = (IntegerX) o;

        String thisString = String.valueOf(x);
        String otherString = String.valueOf(otherIntegerX.x);

        int found = 0;
        String a = thisString + otherString;
        String b = otherString + thisString;

        for (int i = 0; i < a.length(); i++) {
            int result = a.substring(i, i + 1).compareTo(b.substring(i, i + 1));
            if (result != 0) {
                found = result;
                break;
            }
        }


        // Boolean null not found


        // if all characters same, length mismatch

        // 123
        // 12309999
        // 123 1230 <-- bigger
        // 1230 123

        // 123
        // 1239
        // 123 1239
        // 1239 123 <-- bigger

        // 1230
        // 124
        // 124 1230 <--- bigger
        // 1230 124
        return found;
    }

    @Override
    public String toString() {
        return String.valueOf(x);
    }
}

class Solution {
    // 2, 10 = 210
    // 19,18,17, 20
    public String largestNumber(int[] nums) {
        IntegerX[] result = new IntegerX[nums.length];
        for (int i = 0; i < nums.length; i++) {
            result[i] = new IntegerX(nums[i]);
        }
        Arrays.sort(result, Collections.reverseOrder());
        StringBuilder res = new StringBuilder();
        boolean allZero = true;
        for (int i = 0; i < nums.length; i++) {
            if (result[i].x > 0) {
                allZero = false;
            }
            res.append(result[i]);
        }

        if (allZero) {
            return "0";
        }

        return res.toString();
    }
}
// [12321,123]
//  12321123
//  12312321

// 12112
// 12121

class Range {
    int lower, higher;
    Range(int lower, int higher) {
        this.lower = lower;
        this.higher = higher;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Range range = (Range) o;
        return lower == range.lower &&
                higher == range.higher;
    }

    @Override
    public int hashCode() {
        return Objects.hash(lower, higher);
    }
}
