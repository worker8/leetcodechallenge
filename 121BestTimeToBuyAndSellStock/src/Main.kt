import kotlin.random.Random

class Solution {
    fun maxProfit(prices: IntArray): Int {
        var small: Int = Integer.MAX_VALUE
        var big: Int = -1
        var maxDiff = 0

        for (i in 0 until prices.size) {
            val num = prices[i]
            if (small > num) {
                small = num
                big = -1
            } else if (big < num) {
                big = num
            }
            val localDiff = big - small
            if (localDiff > maxDiff) {
                maxDiff = localDiff
            }
            println("small = $small, big = $big, localDiff = $localDiff, maxDiff = $maxDiff")
        }
        if (maxDiff < 0) {
            return 0
        }
        return maxDiff
    }
}

fun randomArray(): IntArray {
    return IntArray(10) { Random.nextInt(10) }
}

fun main() {
    val testInput = intArrayOf(7, 1, 5, 3, 6, 4)
    val testInput2 = intArrayOf(1, 7, 0, 3)
    val testInput3 = randomArray()

    val result = Solution().maxProfit(testInput3)
    println("input: ${testInput3.toList()}")
    println("result: $result")

}