package com.morshe;

public class Main {

    public static void main(String[] args) {
        TreeNode level1 = new TreeNode(3);
        TreeNode level2a = new TreeNode(0);
        TreeNode level2b = new TreeNode(0);
        level1.left = level2a;
        level1.right = level2b;
        Solution solution = new Solution();
        int result = solution.distributeCoins(level1);
        String temp = "";
        temp += 999;
        System.out.println("result: " + temp);
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}


class Solution {
    int counter = 0;

    public int distributeCoins(TreeNode root) {
        counter = 0;
        traverse(root);
        return counter;
    }

    public void traverse(TreeNode root) {
        if (root == null) {
            return;
        }
        traverse(root.left);
        traverse(root.right);

        if (root.left != null && root.left.val != 1) {
            int diff = root.left.val - 1;
            counter += Math.abs(diff);
            root.val += diff;
        }
        if (root.right != null && root.right.val != 1) {
            int diff = root.right.val - 1;
            counter += Math.abs(diff);
            root.val += diff;
        }
    }

}

//    class CoinInfo {
//        int totalValues = 0;
//        int numOfNodes = 0;
//
//        CoinInfo(int totalValues, int numOfNodes) {
//            this.totalValues = totalValues;
//            this.numOfNodes = numOfNodes;
//        }
//    }
//    public CoinInfo traverse(TreeNode root) {//start
//        if (root == null) {
//            return new CoinInfo(0, 0);
//        }
//        int totalValues = 0;
//        int numNodes = 0;
//
//        CoinInfo leftCoin = traverse(root.left);
//        CoinInfo rightCoin = traverse(root.right);
//
//        if (leftCoin.totalValues > leftCoin.numOfNodes) { // we want equal
//            int taken = root.left.val - leftCoin.numOfNodes;
//            root.val += taken;
//            counter += taken;
//        }
//
//        if (rightCoin.totalValues > rightCoin.numOfNodes) { // we want equal
//            int taken = root.right.val - rightCoin.numOfNodes;
//            root.val += taken;
//            counter += taken;
//        }
//        if (root.val > leftCoin.numOfNodes) {
//
//        }
////        if (rightCoin.totalValues < rightCoin.numOfNodes) {
////            int taken2 = root.val - 1;
////            root.val = 1;
////            root.right.val += taken2;
////        }
////        numNodes += rightCoin.numOfNodes;
////        totalValues += rightCoin.totalValues;
//
//        return new CoinInfo(totalValues, numNodes);
//    }// end
//}
