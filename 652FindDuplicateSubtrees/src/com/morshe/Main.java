package com.morshe;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}

class TreeNodeAndCount {
    int count = 0;
    TreeNode node;

    TreeNodeAndCount(TreeNode node, int count) {
        this.node = node;
        this.count = count;
    }

    public void incrementCount() {
        count++;
    }
}

public class Main {
    public static void compare() {

        ArrayList<Integer> list1 = new ArrayList<>();
        ArrayList<Integer> list2 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);

        list2.add(1);
        list2.add(2);
        list2.add(3);

        Map map = new HashMap<ArrayList<Integer>, Integer>();
        map.put(list1, 99);

        Boolean result = list1.size() == list2.size() && list1.containsAll(list2);
        System.out.println("testing array compare: " + result);
        System.out.println("map.get(list1): " + map.get(list1));
        System.out.println("map.get(list2): " + map.get(list2));

    }

    public static void main(String[] args) {
//        compare();
        TreeNode root = new TreeNode(1);
        TreeNode level1_a = new TreeNode(2);
        TreeNode level1_b = new TreeNode(3);

        TreeNode level2_a = new TreeNode(4);
        TreeNode level2_b = new TreeNode(5);
        TreeNode level2_c = new TreeNode(2);
        TreeNode level3_a = new TreeNode(4);
        TreeNode level3_b = new TreeNode(5);

        Map map2 = new HashMap<TreeNode, Integer>();


        root.left = level1_a;
        root.right = level1_b;

        level1_a.left = level2_a;
        level1_a.right = level2_b;
        level1_b.left = level2_c;
        level2_c.left = level3_a;
        level2_c.right = level3_b;

        Solution solution = new Solution();

        List temp = solution.findDuplicateSubtrees(root);
        System.out.println("temp: " + temp);
    }
}

class Solution {
    Map<ArrayList<Integer>, TreeNodeAndCount> map = new HashMap<>();

    public List<TreeNode> findDuplicateSubtrees(TreeNode root) {
        map = new HashMap<>(); // clear it for multiple use
        traverse(root);
        List<TreeNode> resultList = new ArrayList<>();
        // side effect on 'map' variable

        for (Map.Entry<ArrayList<Integer>, TreeNodeAndCount> entry : map.entrySet()) {
            if (entry.getValue().count > 1) {
                resultList.add(entry.getValue().node);
            }
        }
//        System.out.println("list: " + list);
//        System.out.println("map: " + map);
        return resultList;
    }

    public List<Integer> traverse(TreeNode root) {
        ArrayList<Integer> temp = new ArrayList();
        if (root == null) {
            temp.add(null);
            return temp;
        }

        temp.add(root.val);
        temp.addAll(traverse(root.left));
        temp.addAll(traverse(root.right));

        // storing
        if (map.containsKey(temp)) {
            map.get(temp).incrementCount();
        } else {
            map.put(temp, new TreeNodeAndCount(root, 1));
        }
        return temp;
    }
}
