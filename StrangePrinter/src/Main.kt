val testPair = listOf(
//    "aabbccaabb",
//    "abcabcabc",
//    "aaaaaaaaaaaaaaaaaaaaa",
//    "aabbcc",
    "ababa", // correct
    "ababac", //wrong
    "abababac" //wrong
)

class StrangePrinter(val input: CharArray) {
    val output: CharArray = CharArray(input.size)
    var count = 0

    fun print(): String {
        var currentChar = ' '
        for (i in 0 until input.size) {
            if (i == 0) {
                currentChar = input[0]
                output[0] = currentChar
                count++
            } else {
                if (output[i] == input[i]) {
                    continue
                }

                if (currentChar == input[i]) {
                    output[i] = currentChar
                } else if (currentChar != input[i]) {
                    var nextSameCharIndex = -1
                    for (j in input.size - 1 downTo i) {
                        if (currentChar == input[j]) {
                            nextSameCharIndex = j
                            break
                            // output[j] = currentChar
                        }
                    }
                    println("nextSameCharIndex: ${nextSameCharIndex}")
                    if (nextSameCharIndex != -1) {
                        for (j in i..nextSameCharIndex) {
                            output[j] = currentChar
                        }
                    }
                    currentChar = input[i]
                    output[i] = currentChar
                    count++
                }
            }
            println("temp state: ${output.contentToString()}")
        }
        return output.contentToString()
    }
}

fun main() {
//    val input = "aabbccaabb" // able to solve
//    val input = "abcabcabc" // not able to solve
//    val input = "aabcccba" // not able to solve
//    val solution = StrangePrinter(input.toCharArray())
//    println("$input --> ${solution.print()}")
//    println("count: ${solution.count}")

    // DP solution
    testPair.forEach {
        println("[\"${it}\"]: ${Solution2().strangePrinter(it)}")
    }

//    println("answer min: ${hashMap.values}")
}

class Solution2() {
    var testString: String = ""
    val hashMap = hashMapOf<Pair<Int, Int>, Int>()
    fun strangePrinter(s: String): Int {
        testString = s
        return f(0, s.length - 1)
    }

    fun f(start: Int, end: Int): Int {
        //println("(start, end): ($start, $end)")
        if (start == end) {
            return 1
        }

        if (hashMap.containsKey(start to end)) {
            return hashMap.get(start to end)!!
        }
        var min = end + 1
        for (k in start until end) {
            val adjustment = if (testString[k] == testString[end]) {
                1
            } else 0
            val answerK = f(start, k) + f(k + 1, end) - adjustment

            if (answerK < min) {
                hashMap[start to end] = answerK
                min = answerK
            }
        }

        return min
    }
}


// "aba"