package com.morshe;


import javafx.util.Pair;

import java.util.ArrayList;

class Solution {
    public boolean backspaceCompare(String S, String T) {
        return processString(S) == processString(T);
    }

    public String processString(String x) {
        StringBuilder newS = new StringBuilder();
        for (int i = 0; i < x.length(); i++) {
            char c = x.charAt(i);
            if (c != '#') {
                newS.append(c);
            } else {
                if (newS.length() > 0) {
                    newS.deleteCharAt(newS.length() - 1);
                }
            }
        }
        return newS.toString();
    }
}

public class Main {
    public static void main(String[] args) {
        ArrayList<Pair<String, String>> list = new ArrayList<>();
        list.add(new Pair<>("ab#c", "ad#c"));

        Solution solution = new Solution();
        for (Pair<String, String> pair : list) {
            String s = pair.getKey();
            String t = pair.getValue();
            //solution.backspaceCompare(s, t);
            System.out.println(s + ": " + solution.processString(s));
            System.out.println(t + ": " + solution.processString(t));
        }

    }
}
