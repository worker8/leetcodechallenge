package com.morshe;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        int[] nums = {1, 1, 3, 6, 7, 10, 7, 1, 8, 5, 9, 1, 4, 4, 3}; // correct answer = 41
//        int[] nums = {10, 7, 1, 8, 4, 3}; // correct answer = 18, wrong = 21
        Solution solution = new Solution();
        int profit = solution.rob(nums);
        System.out.println("profit: " + profit);
    }
}


class Key {

    private final int x;
    private final int y;

    public Key(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Key)) return false;
        Key key = (Key) o;
        return x == key.x && y == key.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }
}

class Solution {
    // use hashmap to store, what should be the key?
    // the range, I can use string, like "0,2", use Key class, wrote this before hand
    // Key is a range, 0,1 = 0 and 1, inclusive
    Map<Key, Integer> dp = new HashMap<>();

    /*
    abcde
    --    window = 2, times = 4
     --
      --
       --
    */
    public int rob(int[] nums) {
        if (nums.length == 1) {
            return nums[0];
        } else if (nums.length == 0) {
            return 0;
        } else if (nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        } else if (nums.length == 3) {
            int max = Math.max(nums[0], nums[1]);
            return Math.max(max, nums[2]);
        }
        int window = 1;
        for (int i = 0; i < nums.length; i++) {
            dp.put(new Key(i, i), nums[i]);
        }
        window = 2;
        int times = nums.length - window + 1;
        for (int i = 0; i < times; i++) {
            dp.put(new Key(i, i + 1), Math.max(nums[i], nums[i + 1]));
        }
        window = 3;
        times = nums.length - window + 1;
        for (int i = 0; i < times; i++) {
            int max = Math.max(nums[i] + nums[i + 2], nums[i + 1]);
            dp.put(new Key(i, i + 2), max);
        }
//        System.out.println("dp, window 3 = " + dp);
/*
---   window = 3, times = 3
 ---
  ---
----  window = 4, times = 2
 ----
----- window = 5, times = 1
*/
        window = 4;
        while (window < nums.length) { // window = 3, times = 3
            times = nums.length - window + 1;

            for (int j = 0; j < times; j++) {// j = 0
                int max = 0;
                int end = j + window - 1; //inclusive
                for (int i = j; i <= end; i++) { //j=0, window=3
                    // window = 5
                    // i == 0, 0       + Key(2,3)
                    // i == 1, 0       + Key(3,4)
                    // i == 2, Key(0,0)+ Key(4,4)
                    // i == 3, Key(0,1)+ 0
                    // i == 4, Key(1,2)+ 0
                    //abcde
                    //---
                    //^        ^ = mid
                    int left = 0;
                    int mid = nums[i];
                    int right = 0;
                    int subTotal;
                    if (i <= j + 1) {
                        right = dp.get(new Key(i + 2, end));
                        subTotal = left + mid + right;
                    } else if (i > end - 2) {
                        left = dp.get(new Key(j, i - 2));
                        subTotal = left + mid + right;
                    } else { // middle
                        left = dp.get(new Key(j, i - 2));
                        right = dp.get(new Key(i + 2, end));
                        subTotal = left + mid + right;
                    }

                    max = Math.max(max, subTotal);
                }
                dp.put(new Key(j, j + window - 1), max);
            }
            window++;
        }

//        times = nums.length - window + 1;

//        for (int j = 0; j < times; j++) {// j = 0
        int max = 0;
        for (int i = 0; i <= window - 1; i++) { //j=0, window=3
            // window = 5
            // i == 0, 0       + Key(2,3)
            // i == 1, 0       + Key(3,4)
            // i == 2, Key(0,0)+ Key(4,4)
            // i == 3, Key(0,1)+ 0
            // i == 4, Key(1,2)+ 0
            //abcde
            //---
            //^        ^ = mid
            int left = 0;
            int mid = nums[i];
            int right = 0;
            //  0, 1, 2, 3, 4, 5  ; size = 6
            // 10, 7, 1, 8, 4, 3
            // ^
            //01234
            //abcde
            //  ^
            int subTotal = left + mid + right;
            if (i == 0) {
                right = dp.get(new Key(i + 2, nums.length - 2));
                subTotal = left + mid + right;
            } else if (i == 1) {
                right = dp.get(new Key(i + 2, nums.length - 1));
                subTotal = left + mid + right;
            } else if (i == nums.length - 1) {
                left = dp.get(new Key(1, i - 2));
                subTotal = left + mid + right;
            } else if (i == nums.length - 2) {
                left = dp.get(new Key(0, i - 2));
                subTotal = left + mid + right;
            } else {
                if (i > 2) {
                    left = dp.get(new Key(1, i - 2));
                }
                right = dp.get(new Key(i + 2, nums.length - 1));
                subTotal = left + mid + right;
//                System.out.println(i + "a. left: " + left + ", right: " + right + ", mid: " + mid + ", subTotal: " + subTotal);
                left = 0;
                right = 0;
                left = dp.get(new Key(0, i - 2));
                if (i < nums.length - 3) {
                    right = dp.get(new Key(i + 2, nums.length - 2));
                }
                subTotal = Math.max(subTotal, left + mid + right);
//                System.out.println(i + "b. left: " + left + ", right: " + right + ", mid: " + mid + ", subTotal: " + subTotal);
            }

//            System.out.println(i + ": " + subTotal);
            max = Math.max(max, subTotal);
        }
        dp.put(new Key(0, window - 1), max);
//        }
//        System.out.println("dp: " + dp);

        return dp.get(new Key(0, nums.length - 1));
    }
}
