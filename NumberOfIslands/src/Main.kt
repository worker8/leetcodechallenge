class Solution {
    var islandCount = 0
    fun numIslands(grid: Array<CharArray>): Int {
        grid.forEachIndexed { i, row ->
            row.forEachIndexed { j, value ->
                if (grid[i][j] != '0') {
                    traverse(grid, i, j)
                    islandCount++
                }
            }
        }
        return islandCount
    }

    fun traverse(grid: Array<CharArray>, i: Int, j: Int) {
        if (i < 0 || j < 0 || i >= grid.size || j >= grid[0].size || grid[i][j] == '0') {
            return
        }
        // flip to zero after that
        grid[i][j] = '0'
        traverse(grid, i, j + 1)
        traverse(grid, i, j - 1)
        traverse(grid, i + 1, j)
        traverse(grid, i - 1, j)
    }
}

val gridInput = arrayOf(
    charArrayOf('a', 'b', 'c'),
    charArrayOf('0', 'd', '0'),
    charArrayOf('e', 'f', 'g')
)

fun main() {
    val solution = Solution()
    solution.numIslands(gridInput)
    println("\nnumber of island: ${solution.islandCount}")
}