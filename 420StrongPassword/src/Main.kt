class ThreeLetterResult(val isUpperCase: Boolean, val isLowerCase: Boolean, val isDigit: Boolean)
class Solution {
    val MAX_LENGTH = 6

    fun strongPasswordChecker(s: String): Int {
        val threeLetterResult = threeLetterCheck(s)
        return 0
    }

    fun threeRepeatedCheck(s: String, threeLetterResult: ThreeLetterResult) {
        var count = 0

    }

    fun fixing(threeLetterResult: ThreeLetterResult) {
        var temp = ""
        if (!threeLetterResult.isUpperCase) {
            var pointer = 0

        }

    }

    fun threeLetterCheck(s: String): ThreeLetterResult { // must have small case, big case, and number
        var isUpperCase = false
        var isLowerCase = false
        var isDigit = false

        s.forEach {

            if (Character.isUpperCase(it)) {
                isUpperCase = true
            }
            if (Character.isLowerCase(it)) {
                isLowerCase = true
            }
            if (Character.isDigit(it)) {
                isDigit = true
            }
        }
        return ThreeLetterResult(isUpperCase = isUpperCase, isLowerCase = isLowerCase, isDigit = isDigit)
    }
}

fun fixing2(s: String): Int {
    var buffer = ""
    s.forEach {
        if (buffer.length < 3) {
            buffer += it
        } else if (buffer.last() == buffer[buffer.length - 2]) {
            // this is the end of the sentence
            // this is in between - but next and before are the same
            // this is in between - but next and before are NOT the same
        }
    }
    return 0
}

fun main() {
    val testPair = listOf(
        "aabbcc" to 2,
        "aaaaa" to 2,
        "adcc" to 2
    )
    val solution = Solution()
    solution.strongPasswordChecker("aabbcc")
    testPair.forEach { (input, expected) ->
        val actual = solution.strongPasswordChecker(input)
        println("input: ${input}, expected = ${expected}, actual = ${actual}")
    }
    println("-----")
}