class Solution {
    fun maxProfitAssignment(difficulty: IntArray, profit: IntArray, worker: IntArray): Int {
        val store = IntArray(worker.size) { 0 }

        for (wIndex in 0 until worker.size) {
            val w = worker[wIndex]
            for (pindex in 0 until profit.size) {
                val p = profit[pindex]
                if (w >= difficulty[pindex] && store[wIndex] < p) {
                    store[wIndex] = p
                }
            }
        }
        return store.reduce { acc, value -> acc + value }
    }
}

class Solution2 {
    fun maxProfitAssignment(difficulty: IntArray, profit: IntArray, worker: IntArray): Int {
        val store = IntArray(worker.size) { 0 }

        val paired = mutableListOf<Pair<Int, Int>>()
        profit.forEachIndexed { pIndex, p ->
            paired.add(p to pIndex)
        }
        paired.sortBy { (p, pIndex) -> p }

        worker.forEachIndexed { wIndex, w ->
            for (pIndex in paired.size - 1 downTo 0) {
                val (p, dIndex) = paired[pIndex]
                if (w >= difficulty[dIndex]) {
                    store[wIndex] = p
                    break
                }
            }
        }

        return store.reduce { acc, value -> acc + value }
    }
}

fun main() {
    // val solution = Solution()
    val solution = Solution2()
//    val difficulty = intArrayOf(68, 35, 52, 47, 86)
//    val profit = intArrayOf(67, 17, 1, 81, 3)
//    val worker = intArrayOf(92, 10, 85, 84, 82)
    val difficulty = intArrayOf(2, 17, 19, 20, 24, 29, 33, 43, 50, 51, 57, 67, 70, 72, 73, 75, 80, 82, 87, 90)
    val profit = intArrayOf(6, 7, 10, 17, 18, 29, 30, 31, 34, 39, 40, 42, 48, 54, 57, 78, 78, 78, 83, 88)
    val worker = intArrayOf(12, 9, 11, 41, 11, 87, 48, 6, 48, 93, 76, 73, 7, 50, 55, 97, 47, 33, 46, 10)

    println("answer: ${solution.maxProfitAssignment(difficulty, profit, worker)}\n")

}