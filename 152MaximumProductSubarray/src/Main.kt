import kotlin.math.max
import kotlin.random.Random

class Solution {
    var maxStore = Int.MIN_VALUE
    var max2 = Int.MIN_VALUE
    var min2 = Int.MAX_VALUE

    fun maxProduct(array: IntArray): Int {
        maxProd2(array)
        return maxStore
    }

    private fun maxProd2(array: IntArray) {
        max2 = array[array.size - 1]
        min2 = array[array.size - 1]
        if (max2 > maxStore) {
            maxStore = max2
        }
        for (i in array.size - 2 downTo 0) {
            val item = array[i]

            val upper = max2 * item
            val lower = min2 * item
            max2 = Math.max(Math.max(item, upper), lower)
            min2 = Math.min(Math.min(item, upper), lower)

            if (max2 > maxStore) {
                maxStore = max2
            }
        }
    }

    private fun maxProd(array: IntArray, position: Int): Pair<Int, Int> {
        println("position: $position")
        if (array.size - 1 == position) {
            if (array[position] > maxStore) {
                maxStore = array[position]
            }
            return array[position] to array[position]
        } else {
            val item = array[position]
            val nextPair = maxProd(array, position + 1)
            val multiLow = item * nextPair.first
            val multiHigh = item * nextPair.second

            val min = Math.min(Math.min(item, multiLow), multiHigh)
            val max = Math.max(Math.max(item, multiLow), multiHigh)

            if (max > maxStore) {
                maxStore = max
            }
            println("min: $min, max: $max")
            return min to max
        }
    }
}

fun randomIntArray(): IntArray {
    val temp = mutableListOf<Int>()
    (0..10).random()
    repeat(5) { temp.add(Random.nextInt(-10, 10)) }
    return temp.toIntArray()
}

fun main() {
    val testInput = intArrayOf(-2)
    //val testInput1 = randomIntArray()
    println("testInput: ${testInput.toList()}")
    val result = Solution().maxProduct(testInput)
    println("result: $result")
}