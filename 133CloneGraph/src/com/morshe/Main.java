package com.morshe;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

// Definition for a Node.
class Node {
    public int val;
    public List<Node> neighbors;

    public Node() {
    }

    public int getVal() {
        return val;
    }

    public Node(int _val, List<Node> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};


class Solution {
    public Node cloneGraph(Node node) {
        traverseClone(node);
        if (!hashMap.isEmpty()) {
            return hashMap.get(node);
        }
        return null;
    }

    public HashMap<Node, Node> hashMap = new HashMap<>();
    public ArrayList<Node> visited = new ArrayList<>();

    private void traverseClone(Node node) {
        if (!visited.contains(node)) {
            visited.add(node);
            Node newNode;
            if (!hashMap.containsKey(node)) {
                newNode = new Node(node.val, new ArrayList<>());
                hashMap.put(node, newNode);
            } else {
                newNode = hashMap.get(node);
            }

            for (Node neighbor : node.neighbors) {
                Node newNeighborNode;
                if (!hashMap.containsKey(neighbor)) {
                    newNeighborNode = new Node(neighbor.val, new ArrayList<>());
                    hashMap.put(neighbor, newNeighborNode);
                } else {
                    newNeighborNode = hashMap.get(neighbor);
                }
                newNode.neighbors.add(newNeighborNode);
                if (!visited.contains(neighbor)) {
                    traverseClone(neighbor);
                }
            }
        }
    }
}

public class Main {
    public static void traversePrint(Node node) {
        System.out.println("-----");
        List<Node> visitedList = new ArrayList<>();
        _traversePrint(node, visitedList);
        System.out.println("-----");
    }

    private static void _traversePrint(Node node, List<Node> visitedList) {
        if (!visitedList.contains(node)) {
            System.out.println("[" + node.hashCode() + "] " + node.val);
            visitedList.add(node);
            for (Node neighbor : node.neighbors) {
                _traversePrint(neighbor, visitedList);
            }
        }
    }

    public static void main(String[] args) {
        // write your code here
        Node node1 = new Node(11, new ArrayList<>());
        Node node2 = new Node(22, new ArrayList<>());
        Node node3 = new Node(33, new ArrayList<>());
        Node node4 = new Node(44, new ArrayList<>());
        Node node5 = new Node(55, new ArrayList<>());

        node1.neighbors.add(node2);
        node1.neighbors.add(node4);

        node2.neighbors.add(node1);
        node2.neighbors.add(node3);
        node2.neighbors.add(node5);

        node3.neighbors.add(node2);
        node3.neighbors.add(node4);
        node3.neighbors.add(node5);

        node4.neighbors.add(node1);
        node4.neighbors.add(node3);

        node5.neighbors.add(node2);
        node5.neighbors.add(node3);

        traversePrint(node1);
        Solution solution = new Solution();
        Node newGraph = solution.cloneGraph(node1);
        traversePrint(newGraph);
    }
}
