package com.morshe;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Solution solution = new Solution();
        String s1 = "adc";
        String s2 = "dcda";

        boolean answer = solution.checkInclusion(s1, s2);
        System.out.println("answer: " + answer);
    }
}

//Input: s1 = "ab" s2 = "eidbaooo"

// ["a", "b", "a"]
//
// hashMap --->
// "a" : 2 , 0
// "b" : 1 , 0


// loop s2
// 0: e
// 1: i
// 2: d
// 3: b

// hashMap --->
// "a" : 2
// "b" : 0 // remove this entirely

// 4: a

class Count {
    int actual, curr;

    Count(int actual, int curr) {
        this.actual = actual;
        this.curr = curr;
    }

    void inc() {
        curr++;
        actual++;
    }

    void reset() {
        curr = actual;
    }
}

class Solution {
    Map<Character, Count> map = new HashMap<>();

    public boolean checkInclusion(String s1, String s2) {
        // 1. index s1 into a HashMap
        // s1 = ["a", "b", "a"]
        // map
        // "a" --> Count(actual, curr)

        for (int i = 0; i < s1.length(); i++) {
            char c = s1.charAt(i);
            if (!map.containsKey(s1.charAt(i))) {
                map.put(c, new Count(1, 1));
            } else {
                map.get(c).inc();
            }
        }
//        boolean detected = false;
//        for (int i = 0; i < s2.length(); i++) {
//            char c = s2.charAt(i);
//            if (map.containsKey(c)) {
//                detected = true;
//                map.get(c).curr--;
//            } else if (!map.containsKey(c) && detected) {
//                detected = false;
//                restoreMap();
//            }
//            if (isAllIncluded()) {
//                return true;
//            }
//
//        }

        int low = 0;
        int high;
        // "ab", "ooooabee"
        while (low < s2.length()) {
            char c = s2.charAt(low);
            high = low;
            int minusCount = s1.length();
            boolean dirty = false;
            while (map.containsKey(c)) {
                if (map.get(c).curr == 0) {
                    break;
                }
                map.get(c).curr--;
                dirty = true;
                minusCount--;
                if (minusCount == 0) {
                    return true;
                }
                high++;
                if (high >= s2.length()) {
                    break;
                }
                c = s2.charAt(high);
            }
            if (dirty) {
                restoreMap();
            }
            low++;
        }

        return false;
    }

    void restoreMap() {
        for (Map.Entry<Character, Count> entry : map.entrySet()) {
            entry.getValue().reset();
        }
    }

    boolean isAllIncluded() {
        for (Map.Entry<Character, Count> entry : map.entrySet()) {
            if (entry.getValue().curr != 0) {
                return false;
            }
        }
        return true;
    }
}



