package com.morshe;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();
        //int[][] graph = {{1, 3}, {0, 2}, {1, 3}, {0, 2}};
//        int[][] graph = {{1, 2, 3}, {0, 2}, {0, 1, 3}, {0, 2}};
        int[][] graph = {{}, {2, 4, 6}, {1, 4, 8, 9}, {7, 8}, {1, 2, 8, 9}, {6, 9}, {1, 5, 7, 8, 9}, {3, 6, 9}, {2, 3, 4, 6, 9}, {2, 4, 5, 6, 7, 8}};
        boolean answer = solution.isBipartite(graph);
        System.out.println("answer: " + answer);
    }
}

class Solution {
    // if graph = [[1],[0]]
    class Node {
        boolean color;
        Integer val;

        Node(boolean color, Integer val) {
            this.color = color;
            this.val = val;
        }
    }

    public boolean isBipartite(int[][] graph) {
        Set<Integer> visited = new HashSet<>();
        Set<Integer> black = new HashSet<>();
        Set<Integer> white = new HashSet<>();
        LinkedList<Node> queue = new LinkedList<>();

        queue.add(new Node(true, 0));
        white.add(0);

        while (!queue.isEmpty()) {
            // 1. add to visited
            Node curr = queue.poll();
            visited.add(curr.val);
            // 2. for non-visited nodes to queue
            int[] nextNodes = graph[curr.val];
            // - check if they have color conflict, return false if conflict
            // - if not, add to queue
            for (int i = 0; i < nextNodes.length; i++) {
                int nextNode = nextNodes[i];
                if (!visited.contains(nextNode)) {
                    if (curr.color && white.contains(nextNode) ||
                            !curr.color && black.contains(nextNode)) {
//                        System.out.println("black: " + black);
//                        System.out.println("white: " + white);
//                        System.out.println("visited: " + visited);
//                        System.out.println("queue: " + queue);
//                        System.out.println("curr: " + curr);
//                        System.out.println("nextNode: " + nextNode);
//                        System.out.println("-----");
                        return false;
                    } else if (curr.color) {
                        black.add(nextNode);
                    } else if (!curr.color) {
                        white.add(nextNode);
                    }
                    queue.add(new Node(!curr.color, nextNode));
                }
            }
            if (queue.isEmpty() && visited.size() < graph.length) {
                for (int i = 0; i < graph.length; i++) {
                    if (!visited.contains(i)) {
                        queue.add(new Node(true, i));
                        break;
                    }
                }
            }
            // 3. go to next while loop, poll from queue

            // 4. after queue finish return true, no conflict
        }

        return true;
    }
}
