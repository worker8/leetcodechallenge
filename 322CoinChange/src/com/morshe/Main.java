package com.morshe;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] coins = {442, 295, 365, 485};
        int amount = 8091;

//        int[] coins = {10, 5, 7, 11};// 20 + 5 +14 + 11
//        int amount = 51;
        int ans = solution.coinChange(coins, amount);
        System.out.println("answer: " + ans);
        System.out.println("map: " + solution.map);
    }
}

class Solution {
    Integer[] coins;
    Map<Integer, Integer> map = new HashMap<>();

    public int f(int amount) {
        if (map.containsKey(amount)) {
            return map.get(amount);
        }
        if (amount == 0) {
            return 0;
        }
        if (amount < 0) {
            return -1;
        }
        int localMin = Integer.MAX_VALUE;
        for (int i = 0; i < coins.length; i++) {
//            System.out.println("[" + amount + "] subAmount: " + subAmount);
            int ans = f(amount - coins[i]);
//            System.out.println("[" + amount + "] ans: " + ans);
            if (ans == -1) {
                continue;
            } else if (ans >= 0) {
                localMin = Math.min(localMin, ans + 1);
            }
        }
//        System.out.println("[" + amount + "] localMin: " + localMin);
        if (localMin == Integer.MAX_VALUE) {
            map.put(amount, -1);
            return -1;
        } else {
            map.put(amount, localMin);
            return localMin;
        }
    }

    public int coinChange(int[] coins, int amount) {
        this.coins = new Integer[coins.length];
        for (int i = 0; i < coins.length; i++) {
            this.coins[i] = coins[i];
        }
        map.clear();
        Arrays.sort(this.coins, Collections.reverseOrder());

        return f(amount);
    }
}