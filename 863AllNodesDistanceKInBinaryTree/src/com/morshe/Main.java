package com.morshe;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }
}

class Solution {
    Map<TreeNode, TreeNode> parentMap = new HashMap<>(); // node: parentNode (key: value)
    Map<TreeNode, Integer> visitedMap = new HashMap<>(); // node, level (key: value)
    List<Integer> result = new ArrayList<>();

    public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
        parentMap.put(root, null);
        dfs(root);

        bfsWithParent(target, K);
        return result;
    }

    private void bfsWithParent(TreeNode target, int K) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(target);
        visitedMap.put(target, 0);

        while (!queue.isEmpty()) {
            TreeNode curr = queue.poll();
            int currentDistance = visitedMap.get(curr);

            if (currentDistance == K) {
                result.add(curr.val);
            } else if (currentDistance < K) {
                // parent
                TreeNode parent = parentMap.get(curr);
                if (parent != null && !visitedMap.containsKey(parent)) {
                    visitedMap.put(parent, currentDistance + 1);
                    queue.offer(parent);
                }

                // left
                if (curr.left != null && !visitedMap.containsKey(curr.left)) {
                    visitedMap.put(curr.left, currentDistance + 1);
                    queue.offer(curr.left);
                }

                // right
                if (curr.right != null && !visitedMap.containsKey(curr.right)) {
                    visitedMap.put(curr.right, currentDistance + 1);
                    queue.offer(curr.right);
                }
            }
        }
    }

    private void dfs(TreeNode node) {
        if (node.left != null) {
            parentMap.put(node.left, node);
            dfs(node.left);
        }
        if (node.right != null) {
            parentMap.put(node.right, node);
            dfs(node.right);
        }
    }
}


class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}