package com.morshe;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        int temp = -23 % 10;
        System.out.println("Integer.MAX_VALUE: " + Integer.MAX_VALUE);
        System.out.println("Integer.MIN_VALUE: " + Integer.MIN_VALUE);

        List<Integer> list = new ArrayList<>();
        ListIterator<Integer> iter = list.listIterator();

        while (iter.hasNext()) {
            iter.next();
            iter.remove();
        }
    }
}


/**
 * Your WordDictionary object will be instantiated and called as such:
 * WordDictionary obj = new WordDictionary();
 * obj.addWord(word);
 * boolean param_2 = obj.search(word);
 */

class TrieNode {
    HashMap<Character, TrieNode> nodes; // p, u
    Character val;
    boolean isLast;

    TrieNode(Character _val, boolean isLast) {
        this.val = _val;
        this.isLast = isLast;
        this.nodes = new HashMap<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TrieNode trieNode = (TrieNode) o;
        return val.equals(trieNode.val);
    }

    @Override
    public int hashCode() {
        return Objects.hash(val);
    }

    @Override
    public String toString() {
        return String.valueOf(val);
    }
}

class WordDictionary {
    TrieNode head;

    public WordDictionary() {
        head = new TrieNode(' ', false);
    }

    public void addWord(String word) {
        if (word == null || word.isEmpty()) {
            return;
        }
        int i = 0;
        TrieNode curr = head;

        while (i < word.length() - 1) {
            char c = word.charAt(i);
            if (!curr.nodes.containsKey(c)) {
                curr.nodes.put(c, new TrieNode(c, false));
            }
            curr = curr.nodes.get(c);
            i++;
        }

        // handle last character outside the loop, quicker
        char c = word.charAt(word.length() - 1);
        if (!curr.nodes.containsKey(c)) {
            curr.nodes.put(c, new TrieNode(c, true));
        }
        curr.nodes.get(c).isLast = true;
    }

    /**
     * Returns if the word is in the trie.
     */
    public boolean search(String word) {
        if (word == null) {
            return false;
        }
        if (word.isEmpty()) {
            return true;
        }
        List<HashMap<Character, TrieNode>> nextNodes = new ArrayList<>();
        nextNodes.add(head.nodes);

        int i = 0;
        while (i < word.length() - 1) {
            char c = word.charAt(i);
            if (c == '.') {
                List<HashMap<Character, TrieNode>> temp = new ArrayList<>();
                for (HashMap<Character, TrieNode> map : nextNodes) {
                    for (Map.Entry<Character, TrieNode> entry : map.entrySet()) {
                        temp.add(entry.getValue().nodes);
                    }
                }
                nextNodes = temp;
            } else {
                boolean found = false;
                List<HashMap<Character, TrieNode>> temp = new ArrayList<>();
                for (HashMap<Character, TrieNode> map : nextNodes) {
                    if (map.containsKey(c)) {
                        TrieNode nextNode = map.get(c);
                        temp.add(nextNode.nodes);
                        found = true;
                    }
                }
                if (!found) {
                    return false;
                }
                nextNodes = temp;
            }

            i++;
        }
        char c = word.charAt(word.length() - 1);
        if (c == '.' && !nextNodes.isEmpty()) {
            for (HashMap<Character, TrieNode> map : nextNodes) {
                for (Map.Entry<Character, TrieNode> entry : map.entrySet()) {
                    if (entry.getValue().isLast) {
                        return true;
                    }
                }
            }
            return false;
        } else {
            for (HashMap<Character, TrieNode> map : nextNodes) {
                if (map.containsKey(c) && map.get(c).isLast) {
                    return true;
                }
            }
            return false;
        }
    }
}