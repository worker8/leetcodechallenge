package com.morshe;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class Solution {
    private int count = 0;
    private HashMap<Pair<Integer, Integer>, Integer> hashMap = new HashMap<>();

    public int calculate(int x) {
        hashMap.clear();
        return countPossibleCombination(x, x);
        //return count;
    }

    public int countPossibleCombination(int left, int right) {
        if (hashMap.containsKey(new Pair<>(left, right))) {
            return hashMap.get(new Pair<>(left, right));
        }
        if (right <= 0) {
            //count++;
            int ans = 1;
            hashMap.put(new Pair<>(left, right), ans);
            return ans;
        }

        if (left == right) {
            int ans = countPossibleCombination(left - 1, right);
            hashMap.put(new Pair<>(left, right), ans);
            return ans;
        }

        if (left == 0) { // if 'left' empty, but 'right' is not empty
            int ans = countPossibleCombination(0, right - 1);
            hashMap.put(new Pair<>(left, right), ans);
            return ans;
        }

        if (left < right) {
            int ans = countPossibleCombination(left - 1, right) +
                    countPossibleCombination(left, right - 1);
            hashMap.put(new Pair<>(left, right), ans);
            return ans;
        }
        return 0;
    }

    List<String> answer = new ArrayList<>();

    public List<String> generateParenthesis(int n) {
        answer = new ArrayList<>();
        gen(n, n, "");
        return answer;
    }

    private void gen(int left, int right, String holder) {
        System.out.println("left: " + left + ", right: " + right + ", holder: " + holder);
        if (right <= 0) {
            answer.add(holder);
        } else if (left == right) {
            gen(left - 1, right, holder + "(");
        } else if (left <= 0) { // if 'left' empty, but 'right' is not empty
            gen(0, right - 1, holder + ")");
        } else if (left < right) {
            gen(left - 1, right, holder + "(");
            gen(left, right - 1, holder + ")");
        }
    }
}

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        for (int i = 0; i < 4; i++) {
            List<String> s = solution.generateParenthesis(i);
            System.out.println(i + ": " + s + "\n--------------\n");
        }
//        for (int i = 0; i < 10; i++) {
//            System.out.println(i + ": " + solution.calculate(i));
//        }

    }
}
