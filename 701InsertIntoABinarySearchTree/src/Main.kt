/**
 * Example:
 * var ti = TreeNode(5)
 * var v = ti.`val`
 * Definition for a binary tree node.
 * class TreeNode(var `val`: Int) {
 *     var left: TreeNode? = null
 *     var right: TreeNode? = null
 * }
 */

class TreeNode(var `val`: Int) {
    var left: TreeNode? = null
    var right: TreeNode? = null
}

class Solution {
    fun insertIntoBST(root: TreeNode?, `val`: Int): TreeNode? {
        if (root == null) {
            return null
        }
        if (`val` > root.`val`) {
            if (root.right != null) {
                insertIntoBST(root.right, `val`)
            } else {
                root.right = TreeNode(`val`)
            }
        } else if (`val` < root.`val`) {
            if (root.left != null) {
                insertIntoBST(root.left, `val`)
            } else {
                root.left = TreeNode(`val`)
            }
        }
        return root
    }


}

fun printNode(root: TreeNode?) {
    if (root == null) {
        return
    }
    print("${root.`val`},")
    printNode(root.left)
    printNode(root.right)
}

fun main() {
    val root = TreeNode(4)
    val level1a = TreeNode(2)
    val level1b = TreeNode(7)
    val level2a = TreeNode(1)
    val level2b = TreeNode(3)


    root.left = level1a
    root.right = level1b

    level1a.left = level2a
    level1a.right = level2b

    val solution = Solution()
    solution.insertIntoBST(root, 5)
    printNode(root)
}
