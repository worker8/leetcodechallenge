package com.morshe;

import java.util.*;

// 3rd LOOP now - 45 mins each
public class Main {

    public static void main(String[] args) {
        System.out.println("hey there");
        //   A
        //  GE
        // BCD
        List<String> allowed = new ArrayList();
        // set 1
//        allowed.add("CBB");
//        allowed.add("AAD");
//        allowed.add("ABB");
//        allowed.add("ABC");
//        allowed.add("AAA");
//        allowed.add("CDA");
//        allowed.add("ABA");
//        allowed.add("DBC");
//        allowed.add("DBB");
//        allowed.add("DCB");
//        allowed.add("DCC");
//        allowed.add("BCA");
//        allowed.add("BAD");
//        allowed.add("BAB");
//        allowed.add("BAA");
//        allowed.add("DDC");
//        allowed.add("BDA");
//        bottom = "CCA"

        // set 2
//        allowed.add("BCG");
//        allowed.add("CDE");
//        allowed.add("GEA");
//        allowed.add("FFF");
//        bottom = "BCD";

        // set 3
        allowed.add("CBB");
        allowed.add("ACB");
        allowed.add("ABD");
        allowed.add("CDB");
        allowed.add("BDC");
        allowed.add("CBC");
        allowed.add("DBA");
        allowed.add("DBB");
        allowed.add("CAB");
        allowed.add("BCB");
        allowed.add("BCC");
        allowed.add("BAA");
        allowed.add("CCD");
        allowed.add("BDD");
        allowed.add("DDD");
        allowed.add("CCA");
        allowed.add("CAA");
        allowed.add("CCC");
        allowed.add("CCB");
        // bottom = "CCC"

        System.out.println("Solution2: " + new Solution2().pyramidTransition("CCC", allowed));
    }
}

class Solution2 {
    HashMap<String, List<String>> cacheAllowed = new HashMap<>();
    boolean finalFound = false;

    public boolean pyramidTransition(String bottom, List<String> allowed) {
        finalFound = false;

        for (String allowItem : allowed) {
            List<String> found = cacheAllowed.get(allowItem.substring(0, 2));
            if (found != null) {
                found.add(allowItem.substring(2));
            } else {
                ArrayList<String> tempArrayList = new ArrayList<>();
                tempArrayList.add(allowItem.substring(2));
                cacheAllowed.put(allowItem.substring(0, 2), tempArrayList);
            }
        }
        f(bottom, 0, "");

        return finalFound;
    }

    public void f(String bottom, int pos, String nextBottom) {
        if (finalFound) {
            return;
        }
        if (bottom.isEmpty()) {
            return;
        }

        if (pos == bottom.length() - 1) {// BCD, length() = 3, 3 - 1 = 2
            return;
        }

        String key = bottom.substring(pos, pos + 2);
        List<String> found = cacheAllowed.get(key);
        if (found != null) {
            if (bottom.length() == 2) {
                finalFound = true;
            } else if (bottom.length() == pos + 2) { // BCD.length() = 3, pos = 2
                for (String foundItem : found) {
                    f(nextBottom + foundItem, 0, "");
                }
            } else {
                for (String foundItem : found) {
                    f(bottom, pos + 1, nextBottom + foundItem);
                }
            }
        }
    }
}


class Solution {
    public boolean pyramidTransition(String bottom, List<String> allowed) {
        HashMap<Integer, HashMap<Integer, String>> levels = new HashMap();
        levels.put(bottom.length(), new HashMap<>()); // add the first bottom level

        int depth = bottom.length(); // smallest is 1, not 0
        for (int i = 0; i < bottom.length(); i++) {
            levels.get(depth).put(i, String.valueOf(bottom.charAt(i)));
        }

        while (depth != 1 && levels.get(depth) != null && levels.get(depth).size() == depth) {
            HashMap<Integer, String> level = levels.get(depth);
            if (level == null) {
                levels.put(depth, new HashMap<>());
            }
            for (int j = 0; j < level.size() - 1; j++) {

                String a = level.get(j); // this might contain 2 characters, need to be fixed..
                String b = level.get(j + 1);
                System.out.println("--depth: " + depth + "--");
                System.out.println("debug, a = " + a + ", b = " + b);

                for (int xx = 0; xx < a.length(); xx++) {
                    char aChar = a.charAt(xx);
                    for (int yy = 0; yy < b.length(); yy++) {
                        char bChar = b.charAt(yy);

                        System.out.println("debug, aChar = " + aChar + ", bChar = " + bChar);
                        for (String allowedItem : allowed) {
                            if (allowedItem.charAt(0) == aChar && allowedItem.charAt(1) == bChar) {
                                if (levels.get(depth - 1) == null) {
                                    levels.put(depth - 1, new HashMap<>());
                                }

                                if (levels.get(depth - 1).get(j) == null) {
                                    levels.get(depth - 1).put(j, String.valueOf(allowedItem.charAt(2)));
                                } else {
                                    levels.get(depth - 1).put(j, levels.get(depth - 1).get(j) + String.valueOf(allowedItem.charAt(2)));
                                }
                            }
                        }
                    }
                }
            }
            depth--;

            // printing the output
            System.out.println("--levels--");
            System.out.println("levels: " + levels);

        }

        return levels.get(1) != null;

    }
}


// below is copy-paste solution from LeetCode

class SolutionDEMO {
    int[][] T;
    Set<Long> seen;

    public boolean pyramidTransition(String bottom, List<String> allowed) {
        T = new int[7][7];
        for (String a : allowed)
            T[a.charAt(0) - 'A'][a.charAt(1) - 'A'] |= 1 << (a.charAt(2) - 'A');

        seen = new HashSet();
        int N = bottom.length();
        int[][] A = new int[N][N];
        int t = 0;
        for (char c : bottom.toCharArray())
            A[N - 1][t++] = c - 'A';
        return solve(A, 0, N - 1, 0);
    }

    //A[i] - the ith row of the pyramid
    //R - integer representing the current row of the pyramid
    //N - length of current row we are calculating
    //i - index of how far in the current row we are calculating
    //Returns true iff pyramid can be built
    public boolean solve(int[][] A, long R, int N, int i) {
        if (N == 1 && i == 1) { // If successfully placed entire pyramid
            return true;
        } else if (i == N) {
            if (seen.contains(R)) return false; // If we've already tried this row, give up
            seen.add(R); // Add row to cache
            return solve(A, 0, N - 1, 0); // Calculate next row
        } else {
            // w's jth bit is true iff block #j could be
            // a parent of A[N][i] and A[N][i+1]
            int w = T[A[N][i]][A[N][i + 1]];
            // for each set bit in w...
            for (int b = 0; b < 7; ++b)
                if (((w >> b) & 1) != 0) {
                    A[N - 1][i] = b; //set parent to be equal to block #b
                    //If rest of pyramid can be built, return true
                    //R represents current row, now with ith bit set to b+1
                    // in base 8.
                    if (solve(A, R * 8 + (b + 1), N, i + 1)) return true;
                }
            return false;
        }
    }
}
