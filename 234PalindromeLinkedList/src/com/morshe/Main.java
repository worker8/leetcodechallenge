package com.morshe;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
	// write your code here

    }


}


class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; }
}


class Solution {
    ArrayList<Integer> temp;
    // test data
    // 1. nothing or single value
    public boolean isPalindrome(ListNode head) {
        if (head == null || head.next == null) {
            return true;
        }
        temp = new ArrayList<Integer>();
        ArrayList<Integer> tempArray = traverse(head);
        return _isPalindrome(tempArray);
    }

    public boolean _isPalindrome(ArrayList<Integer> nums) {
        // since we only need to loop half
        // 1234321 <---- test data
        // 0123456
        // 0 vs. 6
        // 1 vs. 5
        // 2 vs. 4
        int mid = nums.size() / 2; // test --> 7/2 = 3
        boolean isPalin = true;
        for (int i = 0; i < mid; i++) {
            int j = nums.size() - i - 1;
            if (!nums.get(i).equals(nums.get(j))) {
                isPalin = false;
                break;
            }
        }
        return isPalin;
    }


    private ArrayList traverse(ListNode head) {
        temp.add(head.val);
        if (head.next != null) {
            traverse(head.next);
        }
        return temp;
    }
}
