import kotlin.math.max

class Solution {
    var tempCounter = 0
    var maxArea = 0
    fun maxAreaOfIsland(grid: Array<IntArray>): Int {
        grid.forEachIndexed { i, row ->
            row.forEachIndexed { j, value ->
                if (grid[i][j] != 0) {
                    tempCounter = 0
                    traverse(grid, i, j)
                    if (tempCounter > maxArea) {
                        maxArea = tempCounter
                    }
                }
            }
        }

        return maxArea
    }

    fun traverse(grid: Array<IntArray>, i: Int, j: Int) {
        if (i < 0 || j < 0 || i >= grid.size || j >= grid[0].size || grid[i][j] == 0) {
            return
        }
        // flip to zero after that
        grid[i][j] = 0
        tempCounter++
        traverse(grid, i, j + 1)
        traverse(grid, i, j - 1)
        traverse(grid, i + 1, j)
        traverse(grid, i - 1, j)
    }
}


val gridInput = arrayOf(
    intArrayOf(0, 0, 3, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0),
    intArrayOf(0, 0, 0, 0, 0, 0, 0, 5, 6, 7, 0, 0, 0),
    intArrayOf(0, 9, 10, 0, 13, 0, 0, 0, 0, 0, 0, 0, 0),
    intArrayOf(0, 11, 0, 0, 14, 15, 0, 0, 18, 0, 21, 0, 0),
    intArrayOf(0, 12, 0, 0, 16, 17, 0, 0, 19, 20, 22, 0, 0),
    intArrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 0, 0),
    intArrayOf(0, 0, 0, 0, 0, 0, 0, 24, 26, 28, 0, 0, 0),
    intArrayOf(0, 0, 0, 0, 0, 0, 0, 25, 27, 0, 0, 0, 0)
)


fun main() {
    val solution = Solution()
    println("answer: ${solution.maxAreaOfIsland(gridInput)}")
}