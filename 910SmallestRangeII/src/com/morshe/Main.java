package com.morshe;

class Solution {
    public int smallestRangeII(int[] arr, int k) {
        int N = arr.length - 1;
        int min = arr[0] + k; // 0
        int max = arr[N] - k; // 3
        int answer = max - min; // 3

        for (int i = 1; i < arr.length - 1; i++) {
            int top = arr[i] + k;      //   6
            int bottom = arr[i] - k;   //   0
            int diff1 = top - min;     //  6 - 0  = 6
            int diff2 = max - bottom;  //  3 - 0 = 4
            System.out.println("inputs, " + "top: " + top + ", bottom: " + bottom);
            System.out.println("diff1: " + diff1);
            System.out.println("diff2: " + diff2);
            if (diff1 > diff2) {
                min = bottom;
                answer = diff2;
            } else {
                max = top;
                answer = diff1;
            }
            System.out.println("-----");
        }
        return answer;

    }
}

public class Main {

    public static void main(String[] args) {
        int[] arr = {1, 3, 6};
        int k = 3;
        int answer = new Solution().smallestRangeII(arr, k);
        System.out.println("answer: " + answer);
    }
}
