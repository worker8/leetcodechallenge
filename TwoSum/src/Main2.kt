fun main() {
    val filePath1 = "/file/AAAAA.mp4"
    val filePath2 = "/file/BBBBB.mp4"
    val filePath3 = "/file/CCCCC.mp4"
    val absoluteFilePathList = listOf(filePath1, filePath2, filePath3)
    val fullContent =
        absoluteFilePathList.map { "file '$it'" }.reduce { acc, filePath -> "$acc\n$filePath" }
    println("fullContent: $fullContent")
}