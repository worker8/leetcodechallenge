import java.util.*

class Solution1BruteForce { /* brute force */
    fun twoSum(nums: IntArray, target: Int): IntArray {
        println("nums.size:${nums.size}")
        for (i in 0 until nums.size - 1) {
            val num1 = nums[i]
            for (j in i + 1 until nums.size) {
                val num2 = nums[j]
                if (num1 + num2 == target) {
                    println("SOLUTION: index($i,$j): $num1 + $num2 = $target")
                    return intArrayOf(i, j)
                }
                // println("pair: [i, j] = [$i, $j]")
            }
        }
        return intArrayOf(0, 0)
    }
}

class Solution2HashMap { /* Hash Map */
    fun twoSum(nums: IntArray, target: Int): IntArray {
        val hashMap = hashMapOf<Int, Int>()
        nums.forEachIndexed { index, value ->
            hashMap.put(value, index)
        }
        hashMap.forEach { key, value ->
            println("**(${key}, ${value})")
        }

        nums.forEachIndexed { index, value ->
            val result = target - value
            if (hashMap.containsKey(result) && index != hashMap.get(result)!!) {
                println("** OK: index: ${index}, hashMap.get(result)!!: ${hashMap.get(result)!!}")
                return intArrayOf(index, hashMap.get(result)!!)
            } else {
                println("** NOT OK")
            }
        }
        return intArrayOf(0, 0)
    }
}

class Solution3HashMapOnePass { /* One Pass */
    fun twoSum(nums: IntArray, target: Int): IntArray {
        val hashMap = hashMapOf<Int, Int>()
        nums.forEachIndexed { index, value ->
            val answer = target - value
            if (hashMap.containsKey(answer)) {
                return intArrayOf(index, hashMap[answer]!!)
            }
            hashMap.put(value, index)
        }

        return intArrayOf(0, 0)
    }
}

val testList: List<Pair<IntArray, Int>> = listOf(
    intArrayOf(2, 7, 11, 13) to 9,
    intArrayOf(2, 7, 11, 13) to 20,
    intArrayOf(3, 2, 4) to 6
)

fun main() {
    println("---- Solution 1--- ")
    val solution1BruteForce = Solution1BruteForce()
    testList.forEach { (inputArray, target) ->
        solution1BruteForce.twoSum(inputArray, target)
    }

    println("---- Solution 2--- ")
    val solution2HashMap = Solution2HashMap()
    testList.forEach { (inputArray, target) ->
        val result2 = solution2HashMap.twoSum(inputArray, target)
        println("result2.size: ${result2.size}")
        println("solution2: index(${result2[0]}, ${result2[1]}): = $target")
    }

    println("---- Solution 3--- ")
    val solution3HashMapOnePass = Solution3HashMapOnePass()
    testList.forEach { (inputArray, target) ->
        val result3 = solution3HashMapOnePass.twoSum(inputArray, target)
        println("result3.size: ${result3.size}")
        println("solution3: index(${result3[0]}, ${result3[1]}): = $target")
    }

    println("----end--- ")
}