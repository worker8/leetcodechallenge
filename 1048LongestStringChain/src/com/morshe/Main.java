package com.morshe;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();
        boolean ans1 = solution.isMatch("bcd", "bcde");
        boolean ans2 = solution.isMatch("bcd", "bccX");
        boolean ans3 = solution.isMatch("bcd", "abcd");
        System.out.println("ans1: " + ans1);
        System.out.println("ans2: " + ans2);
        System.out.println("ans3: " + ans3);

        String[] testInput = {"ksqvsyq", "ks", "kss", "czvh", "zczpzvdhx", "zczpzvh", "zczpzvhx", "zcpzvh", "zczvh", "gr", "grukmj", "ksqvsq", "gruj", "kssq", "ksqsq", "grukkmj", "grukj", "zczpzfvdhx", "gru"};
        int finalAnswer = solution.longestStrChain(testInput);
        System.out.println("finalAnswer: " + finalAnswer);
    }
}

// Input: ["a","b","ba","bca","bda","bdca"]
// a - ab - abc - abcd
//          bcd -
class Solution {
    Map<Integer, Set<String>> map = new TreeMap<>();
    Map<String, Integer> counts = new HashMap<>(); // "ba" --> 1, "bca" --> 2

    public int longestStrChain(String[] words) {

        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            if (!map.containsKey(word.length())) {
                map.put(word.length(), new HashSet<>());
            }
            map.get(word.length()).add(word);

        }
        // map -->
        // 1: ["a", "b"]
        // 2: ["ba"]
        // 3: ["bca", "bda"]
        // 4: ["bdca"]

        // counts -->
        // [0] = 1
        // [1] = 1
        // [2] =
        int max = 0;
        for (Map.Entry<Integer, Set<String>> entry : map.entrySet()) {
            for (String word : entry.getValue()) {
                if (word.length() == 1) {
                    counts.put(word, 1);
                } else {
                    int localMax = 1; // can always count itself

                    Set<String> possiblePrevious = map.get(word.length() - 1);
                    if (possiblePrevious != null && !possiblePrevious.isEmpty()) {
                        for (String previous : possiblePrevious) {
                            if (isMatch(previous, word)) {
                                if (counts.containsKey(previous)) {
                                    localMax = Math.max(localMax, 1 + counts.get(previous));
                                }
                            }
                        }
                    }
                    counts.put(word, localMax);
                    max = Math.max(max, localMax);
                }

            }
        }


        return max;
    }
    // first - bcd
    // second - bcde  ==> true

    // first - bcd
    // second - ebcd  ==> true

    // first - bcd
    // second - bXcd  ==> false
    boolean isMatch(String first, String second) { // second is the longer string, longer by only 1
        int singleSkip = 0;

        int i = 0;
        while (i < first.length()) {
            if (first.charAt(i) == second.charAt(i + singleSkip)) {
                i++;
                continue;
            } else if (first.charAt(i) != second.charAt(i + singleSkip) && singleSkip == 0) {
                singleSkip++;
                continue;
            } else if (first.charAt(i) != second.charAt(i + singleSkip) && singleSkip == 1) {
                return false;
            }
            i++;
        }
        return true;
    }
}