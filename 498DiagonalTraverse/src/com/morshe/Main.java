package com.morshe;

public class Main {

    public static void main(String[] args) {
//        int[][] matrix = {
//                {1, 2, 3},
//                {4, 5, 6},
//                {7, 8, 9}
//        };
//        { 1,  2,  3,  4},
//        { 5,  6,  7,  8},
//        { 9, 10, 11, 12},

//        {  1,  2,  3,  4},
//        {  5,  6,  7,  8},
//        {  9, 10, 11, 12},
//        { 13, 14, 15, 16},
        int[][] matrix = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16},
        };
        Solution solution = new Solution();
        solution.findDiagonalOrder(matrix);

        int count = 0;
        System.out.println("count: " + ++count);
    }
}

class Solution {
    public int[] findDiagonalOrder(int[][] matrix) {
        int m = 0, n = 0;
        if (matrix.length == 0) {
            return new int[0];
        }
        if (matrix.length == 1) {
            return matrix[0];
        }

        // ------>M
        // |
        // |
        // N

        int M = matrix[0].length;
        int N = matrix.length;
        int total = M * N;
        int[] result = new int[total];
        boolean direction = true; // true = up + right, false = down + left
        int count = 0;
        while (count < total) {
//            System.out.println("n,m = " + n + "," + m + ": " + matrix[n][m] + ", direction: " + direction);
            result[count++] = matrix[n][m];
            if (direction) {
                if (m < M - 1 && n > 0) {
                    m++;
                    n--;
                } else if (n == 0 && m < M - 1) {
                    m++;
                    direction = false;
                } else if (m == M - 1) {
                    n++;
                    direction = false;
                }
            } else {
                if (n < N - 1 && m > 0) {
                    n++;
                    m--;
                } else if (m == 0 && n < N - 1) {
                    n++;
                    direction = true;
                } else if (n == N - 1) {
                    m++;
                    direction = true;
                }
            }
        }
//        System.out.print("[");
//        for (int i = 0; i < result.length; i++) {
//            System.out.print("" + result[i] + ", ");
//        }
//        System.out.println("]");
        return result;
    }
}