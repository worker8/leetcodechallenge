package com.morshe;

import java.sql.SQLSyntaxErrorException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }


}

//class Solution { /* slow!! */
//    HashMap<Integer, ArrayList> hashMap = new HashMap<>();
//
//    public List<List<Integer>> levelOrder(TreeNode root) {
//        ArrayList<List<Integer>> outerList = new ArrayList();
//        levelOrder2(root, 0);
//        hashMap.forEach((level, list) -> {
//            outerList.add(list);
//            System.out.println(level + ": " + list);
//        });
//        return outerList;
//    }
//
//
//    private void levelOrder2(TreeNode root, int level) {
//        if (root == null) {
//            return;
//        }
//        if (hashMap.containsKey(level)) {
//            ArrayList list = hashMap.get(level);
//            list.add(root.val);
//        } else {
//            ArrayList list = new ArrayList<>();
//            list.add(root.val);
//            hashMap.put(level, list);
//        }
//
//        levelOrder2(root.left, level + 1);
//        levelOrder2(root.right, level + 1);
//    }
//}

class Solution { /* slow!! */
    ArrayList<List<Integer>> answerList = new ArrayList<>();

    public List<List<Integer>> levelOrder(TreeNode root) {

        levelOrder2(root, 0);
//        hashMap.forEach((level, list) -> {
//            outerList.add(list);
//            System.out.println(level + ": " + list);
//        });
        return answerList;
    }


    private void levelOrder2(TreeNode root, int level) {
        if (root == null) {
            return;
        }
        if (answerList.size() > level) {
            ArrayList list = (ArrayList) answerList.get(level);
            list.add(root.val);
        } else {
            ArrayList list = new ArrayList<>();
            list.add(root.val);
            answerList.add(list);
        }

        levelOrder2(root.left, level + 1);
        levelOrder2(root.right, level + 1);
    }
}

public class Main {

    public static void main(String[] args) {
        // write your code here
        TreeNode node1 = new TreeNode(3);
        TreeNode node2 = new TreeNode(9);
        TreeNode node3 = new TreeNode(20);
        TreeNode node4 = new TreeNode(15);
        TreeNode node5 = new TreeNode(7);
        node1.left = node2;
        node1.right = node3;
        node3.left = node4;
        node3.right = node5;

        Solution solution = new Solution();
        List list = solution.levelOrder(node1);
        System.out.println("----");
        System.out.println(list);
    }
}
