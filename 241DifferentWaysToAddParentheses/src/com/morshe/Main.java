package com.morshe;

import javafx.util.Pair;

import java.lang.reflect.Array;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
        String testInput = "2*31-4*5";
        Solution solution = new Solution();
        List<Integer> answer = solution.diffWaysToCompute(testInput);
        System.out.println("answer: " + answer);
    }
}

class Solution {
    List<Integer> items = new ArrayList<>();
    List<Character> operators = new ArrayList<>();

    void parseInput(String input) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == '*' || c == '+' || c == '-') {
                operators.add(c);
                items.add(Integer.valueOf(sb.toString()));
                sb.setLength(0);
            } else if (i == input.length() - 1) {
                sb.append(c);
                items.add(Integer.valueOf(sb.toString()));
                sb.setLength(0);
            } else {
                sb.append(c);
            }
        }
    }

    int calculate(Integer left, char operator, Integer right) {
        if (operator == '*') {
            return left * right;
        } else if (operator == '-') {
            return left - right;
        } else {
            return left + right;
        }
    }

    Map<String, List<Integer>> dp = new HashMap<>();

    public List<Integer> diffWaysToCompute(String input) {
        parseInput(input);
        // items: [2, 31, 4, 5]
        // operators: [*, -, *]
        if (items.size() == 1) {
            return items.subList(0, 1);
        } else if (items.size() == 2) {
            return Arrays.asList(calculate(items.get(0), operators.get(0), items.get(1)));
        }

        // window == 2
        int window = 1;
        for (int i = 0; i < items.size(); i++) {
            List<Integer> answer = Arrays.asList(items.get(i));
            String key = i + "," + (i + window - 1);
            dp.put(key, answer);
        }

        window = 2;
        for (int i = 0; i < items.size() - 1; i++) {
            List<Integer> answer = Arrays.asList(calculate(items.get(i), operators.get(i), items.get(i + window - 1)));
            String key = i + "," + (i + window - 1);
            dp.put(key, answer);
        }
        System.out.println("after window = 2, dp = " + dp);
        // items.size() = 4
        // window = 3
        for (window = 3; window <= items.size(); window++) {
            for (int j = 0; j + window - 1 < items.size(); j++) {
                // j = 012, window = 3
                // j = 123, window = 3
                // i = ^
                int windowStart = j;
                int windowEnd = j + window - 1;
                for (int i = windowStart; i <= windowEnd; i++) {
                    System.out.println("window = " + window + ", j = " + j + ", i = " + i);
                    List<Integer> leftList;
                    List<Integer> rightList;
                    Integer mid = items.get(i);
                    List<Integer> result = new ArrayList<>();
                    if (i == 0) {
                        rightList = dp.get((i + 1) + "," + windowEnd);
                        for (Integer numRight : rightList) {
                            result.add(calculate(mid, operators.get(i), numRight));
                        }
                    } else if (windowStart < i && i < windowEnd) {
                        leftList = dp.get(windowStart + "," + (i - 1));
                        rightList = dp.get((i + 1) + "," + windowEnd);
                        for (Integer numLeft : leftList) {
                            for (Integer numRight : rightList) {
                                int temp = calculate(numLeft, operators.get(i - 1), mid);
                                result.add(calculate(temp, operators.get(i), numRight));
                            }
                        }
                    } else if (i == windowEnd) {
                        leftList = dp.get(windowStart + "," + (i - 1));
                        for (Integer numLeft : leftList) {
                            result.add(calculate(numLeft, operators.get(i - 1), mid));
                        }
                    }
                    String key = windowStart + "," + windowEnd;
                    List<Integer> found = dp.get(key);
                    if (found == null) {
                        found = new ArrayList<>();
                    }
                    found.addAll(result);
                    dp.put(key, found);
                    System.out.println("---------");
                }
            }
        }
        System.out.println("after All, dp = " + dp);
        return dp.get(0 + "," + (items.size() - 1));
    }
}

/*
class Solution2 {
    // key = "2*3", value = [6]
    // key = "2*3-4", value = [2, -2]
    Map<String, List<Integer>> dp;

    public List<Integer> diffWaysToCompute(String input) {

        // parse
        //int numOfOperand = 0;
        List<String> items = new ArrayList<>(); // ["2", "*", "3"...]

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == '*' || c == '+' || c == '-') {
                items.add(sb.toString());
                items.add(String.valueOf(c));
                sb.setLength(0);
            } else if (i == input.length() - 1) {
                items.add(String.valueOf(c));
            } else {
                sb.append(c);
            }
        }

        System.out.println("check parsing: " + items);
        dp = new HashMap<>();

        // 0 1 2 3 4 5 6 7 8
        // 1 + 2 * 3 - 4 + 5
        for (int window = 3; window <= items.size(); window += 2) {
            // window == 7
            // 0,1,2,3,4,5,6 = 1 + 2 * 3 - 4
            //                 1 + ((2 * 3) - 4)
            //                 1 + (2 * (3 - 4))
            //                 (1 + 2) * (3 - 4)
            //                 (1 + (2 * 3)) - 4
            //                 ((1 + 2) * 3) - 4
            // 2,3,4,5,6,7,8 = 2 * 3 - 4 + 5
            // window == 5
            // 0,1,2,3,4 = 1 + 2 * 3
            // 2,3,4,5,6 = 2 * 3 - 4
            // 4,5,6,7,8 = 3 - 4 + 5
            // window == 3
            // 0,1,2 = 1 + 2
            // 2,3,4 = 2 * 3
            // 4,5,6 = 3 - 4
            // 6,7,8 = 4 + 5
            for (int j = 0; j + window - 1 < items.size(); j += 2) {
                // j => 0,2,4,6,8
                // j.. j + window = 012, 234,456, 678
                if (window == 3) {
                    int ans = calculate(Integer.valueOf(items.get(j)), items.get(j + 1), Integer.valueOf(items.get(j + 2)));
                    StringBuilder tempSb = new StringBuilder();
                    tempSb.append(items.get(j));
                    tempSb.append(items.get(j + 1));
                    tempSb.append(items.get(j + 2));
                    dp.put(tempSb.toString(), Arrays.asList(ans));
                    continue;
                }

                // j => 0,2
                // j.. j + window = 0~6, 2~8
                // 0,1,2,3,4,5,6 = 1 + 2 * 3 - 4
                // 2,3,4,5,6,7,8 = 2 * 3 - 4 + 5
                // ^
                // k
                List<Integer> left = null;
                String leftOperator = null;
                Integer mid = null;
                String rightOperator = null;
                List<Integer> right = null;
                for (int k = j; k <= j + window - 1; k += 2) {
                    System.out.println("k: " + k);
                    // k = 0,2,4,6
                    if (k == 2) {
                        left = Arrays.asList(Integer.valueOf(items.get(0)));
                        leftOperator = items.get(k - 1);
                        rightOperator = items.get(k + 1);
                    } else if (k > 2) {
                        left = dp.get(buildString(items, j, k - 1));
                        leftOperator = items.get(k - 1);
                        rightOperator = items.get(k + 1);
                    }
                    if (k == j + window - 3) {
                        right = Arrays.asList(Integer.valueOf(items.get(items.size() - 1)));
                        leftOperator = items.get(k - 1);
                        rightOperator = items.get(k + 1);
                    } else if (k < j + window - 3) {
                        right = dp.get(buildString(items, k, j + window));
                        leftOperator = items.get(k - 1);
                        rightOperator = items.get(k + 1);
                    }

                    mid = Integer.valueOf(items.get(k));

                    if (left != null && right != null) {
                        List<Integer> tempAns = crossFunctionSingle(left, mid, leftOperator);
                        List<Integer> ans = crossFunction(tempAns, right, rightOperator);
                        dp.put(buildString(items, k, k + window), ans);
                    } else if (left == null && right != null) {
                        List<Integer> ans = crossFunctionSingle(right, mid, leftOperator);
                        dp.put(buildString(items, k, k + window), ans);
                    } else if (left != null && right == null) {
                        List<Integer> ans = crossFunctionSingle(left, mid, leftOperator);
                        dp.put(buildString(items, k, k + window), ans);
                    }
                }
            }
        }

        return dp.get(input);
    }

    String buildString(List<String> items, int startIndex, int endIndex) {
        StringBuilder sb = new StringBuilder();
        for (int i = startIndex; i < endIndex; i++) {
            sb.append(items.get(i));
        }
        return sb.toString();
    }

    List<Integer> crossFunctionSingle(List<Integer> first, Integer mid, String operator) {
        List<Integer> result = new ArrayList<>();
        for (Integer item1 : first) {
            result.add(calculate(item1, operator, mid));
        }
        return result;
    }

    List<Integer> crossFunction(List<Integer> first, List<Integer> second, String operator) {
        List<Integer> result = new ArrayList<>();
        for (Integer item1 : first) {
            for (Integer item2 : second) {
                result.add(calculate(item1, operator, item2));
            }
        }
        return result;
    }

    int calculate(Integer left, String operator, Integer right) {
        if (operator.equals("*")) {
            return left * right;
        } else if (operator.equals("-")) {
            return left - right;
        } else {
            return left + right;
        }
    }
}*/
