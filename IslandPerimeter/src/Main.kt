class Solution {
    var count = 0
    fun islandPerimeter(grid: Array<IntArray>): Int {
        grid.forEachIndexed { i, row ->
            row.forEachIndexed { j, value ->
                if (grid[i][j] != 0) {
                    dfs(grid, i, j)
                }
            }
        }
        return count
    }

    fun dfs(grid: Array<IntArray>, i: Int, j: Int) {
        if (i < 0 || j < 0 || i >= grid.size || j >= grid[0].size || grid[i][j] == 0) {
            count++
            return
        }
        if (grid[i][j] == 2) {
            return
        }
        grid[i][j] = 2
        dfs(grid, i + 1, j)
        dfs(grid, i - 1, j)
        dfs(grid, i, j + 1)
        dfs(grid, i, j - 1)
    }
}

val testGrid = arrayOf(
    intArrayOf(0, 1, 0, 0),
    intArrayOf(1, 1, 1, 0),
    intArrayOf(0, 1, 0, 0),
    intArrayOf(1, 1, 0, 0)
)

fun main() {
    println("answer: ${Solution().islandPerimeter(testGrid)}")
}