package com.morshe;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }
}

// Trie trie = new Trie();
// trie.insert("apple");
// trie.search("apple");   // returns true
// trie.search("app");     // returns false
// trie.startsWith("app"); // returns true
// trie.insert("app");
// trie.search("app");     // returns true
// This is Trie type data structure

// root --> nodes==> 'a': TrieNode('p') nodes ==> 'p'

class TrieNode {
    HashMap<Character, TrieNode> nodes; // p, u
    Character val;
    boolean isLast;

    TrieNode(Character _val, boolean isLast) {
        this.val = _val;
        this.isLast = isLast;
        this.nodes = new HashMap<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TrieNode trieNode = (TrieNode) o;
        return val.equals(trieNode.val);
    }

    @Override
    public int hashCode() {
        return Objects.hash(val);
    }
}

class Trie {
    TrieNode head;

    /**
     * Initialize your data structure here.
     */
    public Trie() {
        head = new TrieNode(' ', false);
    }

    /**
     * Inserts a word into the trie.
     */
    public void insert(String word) {
        if (word == null || word.isEmpty()) {
            return;
        }
        int i = 0;
        TrieNode curr = head;

        //     TrieNode(' ')
        //        /
        //   'a': TN('a')
        //      /
        //  'p': TN('p')

        while (i < word.length() - 1) {
            char c = word.charAt(i);
            if (!curr.nodes.containsKey(c)) {
                curr.nodes.put(c, new TrieNode(c, false));
            }
            curr = curr.nodes.get(c);
            i++;
        }

        // handle last character outside the loop, quicker
        char c = word.charAt(word.length() - 1);
        if (!curr.nodes.containsKey(c)) {
            curr.nodes.put(c, new TrieNode(c, true));
        } else {
            curr.nodes.get(c).isLast = true;
        }
    }

    /**
     * Returns if the word is in the trie.
     */
    public boolean search(String word) {
        if (word == null) {
            return false;
        }
        if (word.isEmpty()) {
            return true;
        }

        //        'a' <--- curr
        //      /    \
        //     'p'  'u' <---- HashMap
        //     /
        //    'p' (end)
        TrieNode curr = head;
        int i = 0;
        while (i < word.length() - 1) {
            char c = word.charAt(i);
            if (curr.nodes.isEmpty()) {
                return false;
            }
            if (!curr.nodes.containsKey(c)) { // if cannot find c in nodes, it means cannot be found!
                return false;
            } else {
                curr = curr.nodes.get(c);
            }
            i++;
        }

        char c = word.charAt(i); // i == word.length() - 1
        if (!curr.nodes.containsKey(c)) { // if cannot find c in nodes, it means cannot be found!
            return false;
        } else {
            return curr.nodes.get(c).isLast;
        }
    }

    /**
     * Returns if there is any word in the trie that starts with the given prefix.
     */
    public boolean startsWith(String prefix) {
        if (prefix == null) {
            return false;
        }
        if (prefix.isEmpty()) {
            return true;
        }

        //        'a' <--- curr
        //      /    \
        //     'p'  'u' <---- HashMap
        //     /
        //    'p' (end)
        TrieNode curr = head;
        int i = 0;
        while (i < prefix.length()) {
            char c = prefix.charAt(i);
            if (curr.nodes.isEmpty()) {
                return false;
            }
            if (!curr.nodes.containsKey(c)) { // if cannot find c in nodes, it means cannot be found!
                return false;
            } else {
                curr = curr.nodes.get(c);
            }
            i++;
        }
        return true;
    }
}