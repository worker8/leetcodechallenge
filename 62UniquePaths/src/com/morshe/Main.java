package com.morshe;

import java.util.HashMap;
import java.util.Map;

class TwoPoint {
    int m;
    int n;

    TwoPoint(int m, int n) {
        this.m = m;
        this.n = n;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof TwoPoint)) {
            return false;
        }

        TwoPoint temp = (TwoPoint) obj;
        return temp.m == m &&
                temp.n == n;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + m;
        result = 31 * result + n;
        return result;

    }

    @Override
    public String toString() {
        return "(" + m + "," + n + ")";
    }
}

class Solution {
    Map<TwoPoint, Integer> store = new HashMap();

    public int uniquePaths(int m, int n) { // 3, 2
        if (m == 1 || n == 1) {
            return 1;
        }
        TwoPoint key = new TwoPoint(m, n);
        int result;
        Integer cache = store.get(key);
        if (cache != null) {
            System.out.println("hit!");
            result = cache;
        } else {
            System.out.println("miss... " + key);
            result = uniquePaths(m - 1, n) + uniquePaths(m, n - 1);
            store.put(key, result);
        }
        System.out.println("store: " + store);
        return result;
    }
}

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();
        int result = solution.uniquePaths(7, 3);
        System.out.println("result: " + result);
    }
}
