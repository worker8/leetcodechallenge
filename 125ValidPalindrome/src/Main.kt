import kotlin.system.measureTimeMillis

class Solution {
    fun cleanString(s: String): String {
        var tempString = ""
        s.forEach {
            if (it.isLetterOrDigit()) {
                tempString += it
            }
        }
        return tempString.toLowerCase()
    }

}
fun isPalindrome(s: String): Boolean {
    val cleaned = cleanString(s)
    if (cleaned.isEmpty()) {
        return true
    }
    if (cleaned.length == 1) {
        return true
    }
    var temp = cleaned
    if (temp.length % 2 == 1) { // isOdd = true
        val tempMid = temp.length / 2
        temp = cleaned.removeRange(tempMid, tempMid + 1)
    }

    var isPalin = true

    val mid2 = temp.length / 2 // 2, abba
//        println("temp: $temp, mid2 = $mid2")
    for (i in 0 until mid2) { // 4 / 2 = 2
        val head = temp[i]
        val tail = temp[temp.length - i - 1]
//            println("head = $head, tail = $tail")
        if (head != tail) {
            isPalin = false
            break
        }
    }
    return isPalin
}

class Solution2 {
    fun cleanString(s: String): String {
        var tempString = ""
        s.forEach {
            if (it.isLetterOrDigit()) {
                tempString += it
            }
        }
        return tempString.toLowerCase()
    }

    fun isPalindrome(input: String): Boolean {
        val s = cleanString(input)
        var isPalindrome = true

        val mid = s.length / 2 // it will round down, confirm?
        for (i in 0 until mid) {
            val first = s[i]
            val second = s[s.length - 1 - i]

            if (first != second) {
                isPalindrome = false
                break
            }
        }
        return isPalindrome
    }
}

class Solution3 {
    fun isPalindrome(input: String): Boolean {
        if (input.length <= 1) {
            return true
        }
        var isPalindrome = true
        var upPointer = 0
        var downPointer = input.length - 1


        // check for case
        // ""    // 0, -1  //done
        // "a"   // 0, 0  //done
        // "ab"  // 0, 1
        // "aba" // 0, 2
        while (downPointer > upPointer) {
            if (!input[upPointer].isLetterOrDigit()) {
                upPointer++
            } else if (!input[downPointer].isLetterOrDigit()) {
                downPointer--
            } else {
                if (input[upPointer].toLowerCase() != input[downPointer].toLowerCase()) {
                    isPalindrome = false
                    break
                }
                upPointer++
                downPointer--
            }
        }
        return isPalindrome
    }
}

// 1 loop
fun cleanString(s: String): String {
    var tempString = ""
    s.forEach {
        if (it.isLetter()) {
            tempString += it
        }
    }
    return tempString.toLowerCase()
}

fun main() {//
    val testArray = arrayListOf(
        "A man, a plan, a canal: Panama",
        "OP",
        "ab",
        "a",
        "",
        "cnn3oihiohch",
        "aBbbba",
        "c3dd3c",
        "c3dXd3c",
        "aBa"
    )
    val time = measureTimeMillis {
        repeat(1000000){
            testArray.forEach { testString ->
                val result = Solution3().isPalindrome(testString)
//                println("isPalindrome($testString) = $result")
//                println("------")
            }
        }
    }

    println("time: $time")

//    val result = Solution3().isPalindrome("ab,Ba")
//    println("result = $result")
}