package com.morshe;

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}

class Solution {
    private void invert(TreeNode root) {
        if (root != null && root.left != null && root.right != null) {
            TreeNode temp;
            temp = root.left;
            root.left = root.right;
            root.right = temp;
            invert(root.left);
            invert(root.right);
        }
    }

    public TreeNode invertTree(TreeNode root) {
        invert(root);
        return root;
    }
}

public class Main {

    private static void _traverse(TreeNode treeNode) {
        if (treeNode != null) {
            if (treeNode.left != null) {
                System.out.println(treeNode.left.val);
            }
            if (treeNode.right != null) {
                System.out.println(treeNode.right.val);
            }
            _traverse(treeNode.left);
            _traverse(treeNode.right);
        }
    }

    public static void traverse(TreeNode treeNode) {
        System.out.println(treeNode.val);
        _traverse(treeNode);

        System.out.println("--------");
    }

    public static void main(String[] args) {
        TreeNode node1 = new TreeNode(4);
        TreeNode node2 = new TreeNode(2);
        TreeNode node3 = new TreeNode(7);

        TreeNode node4 = new TreeNode(1);
        TreeNode node5 = new TreeNode(3);
        TreeNode node6 = new TreeNode(6);
        TreeNode node7 = new TreeNode(9);

        node1.left = node2;
        node1.right = node3;
        node2.left = node4;
        node2.right = node5;

        node3.left = node6;
        node3.right = node7;

        traverse(node1);
        Solution solution = new Solution();
        solution.invertTree(node1);
        traverse(node1);
    }
}
