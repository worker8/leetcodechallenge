package com.morshe;

import java.util.*;

public class Main {

    public static void main(String[] args) {
//        int[][] graph = {{1, 2}, {3}, {3}, {}};
        Set<Integer> set = new HashSet<>();
        
        int[][] graph = {{3, 10, 9, 7, 4, 8, 1, 2, 5, 6}, {2, 5, 10, 9, 7, 3, 6}, {3, 4, 8, 5, 7, 10, 9, 6}, {4, 5, 10, 6, 9, 7}, {6, 10, 5, 7}, {10, 8, 9, 7, 6}, {10, 8, 9, 7}, {10, 8}, {10, 9}, {10}, {}};

//        for (int i = 0; i < graph.length; i++) {
//            for (int j = 0; j < graph[i].length; j++) {
//                int item = graph[i][j];
//                System.out.println(item);
//            }
//        }
        System.out.println("----");
        Solution solution = new Solution();

        List<List<Integer>> res = solution.allPathsSourceTarget(graph);
        System.out.println("res: " + res);
    }
}

//          0      1     2    3    4
//Input: [[1,2], [3,4], [4], [4], []]
//        Output: [[0,1,4],[0,2,4],[0,1,3,4]]
//        Explanation: The graph looks like this:
//        0--->1--->3
//        |    |   /
//        v    v  /
//        2--->4<-

//          0      1     2    3
//Input: [[1,2], [2,3], [3], []]
//        Output: [[0,1,2,3],[0,1,3],[0,2,3]]
//        Explanation: The graph looks like this:
//        0---->1
//        |   / |
//        v ム  v
//        2---->3
//       hashMap
//       0 - []
//       1 - [0]
//       2 - [0,1]
//       3 - [1,2]
//   f(3) = f(1) + f(2) = [[0,1][]]
//   f(1) = [1] + [0] = [0,1] // add in reverse
//class Solution {
//    int[][] graph;
//
//    // return - read as List<of Paths>, List<Integers> forms a Path
//    public List<List<Integer>> allPathsSourceTarget(int[][] graph) {
//        this.graph = graph;
//        List<List<Integer>> list = new ArrayList<>();
//        f(0, new ArrayList<>()); // start from the root point
//
//        return list;
//    }
//
//    // f(1) = [[2,3], [3]]
//    // f(2) = [[3]]
//    List<List<Integer>> f(int pos, List<List<Integer>> paths) {
//        int[] nextPoints = graph[pos];
//
//        for (int i = 0; i < paths.size(); i++) {
//            List<Integer> path = paths.get(i);
//            path.add(pos);
//        }
//
//        if (paths.isEmpty()) {
//            List<Integer> path = new ArrayList<>();
//            path.add(pos);
//            paths.add(path);
//        }
//
//        for (int i = pos; i < nextPoints.length; i++) {
//            f(i, paths);
//        }
//    }
//
//    List<List<Integer>> crossPlus(List<List<Integer>> a1, List<List<Integer>> a2) {
//        List<List<Integer>> result = new ArrayList<>();
//
//        for (List<Integer> item1 : a1) {
//            for (List<Integer> item2 : a2) {
//                List<Integer> tempList = new ArrayList<>();
//                tempList.addAll(item1);
//                tempList.addAll(item2);
//                result.add(tempList);
//            }
//        }
//        return result;
//    }
//}
//Input: [[1,2], [3], [3], []]
//        Output: [[0,1,3],[0,2,3]]
//        Explanation: The graph looks like this:
//        0--->1
//        |    |
//        v    v
//        2--->3
class Solution {
    private int[][] graph;
    private Map<Integer, List<List<Integer>>> map = new HashMap<>();

    //    {{3, 10, 9, 7, 4, 8, 1, 2, 5, 6}, {2, 5, 10, 9, 7, 3, 6}, {3, 4, 8, 5, 7, 10, 9, 6}, {4, 5, 10, 6, 9, 7}, {6, 10, 5, 7}, {10, 8, 9, 7, 6}, {10, 8, 9, 7}, {10, 8}, {10, 9}, {10}, {}};
    public List<List<Integer>> allPathsSourceTarget(int[][] graph) {
        this.graph = graph;
        return f(0);
    }

    private List<List<Integer>> f(int position) {
        //memoization
        List<List<Integer>> found = map.get(position);
        if (found != null) {
            return found;
        }

        // case 2: this is the endpoint, the last point to be visited
        if (graph[position].length == 0) {
            List<Integer> path = new ArrayList<>();
            path.add(position);
            List<List<Integer>> paths = new ArrayList<>();
            paths.add(path);
            return paths;
        }

        // case 1: when not the endpoint, traverse through all the possible points
        List<List<Integer>> acc = new ArrayList<>(); // acc = accumulator

        for (int j = 0; j < graph[position].length; j++) {
            List<List<Integer>> nextPaths = f(graph[position][j]);
            List<List<Integer>> currAndNextPaths = addPointToPath(position, nextPaths);
            acc.addAll(currAndNextPaths);
        }
        map.put(position, acc);
        return acc;
    }

    private List<List<Integer>> addPointToPath(int position, List<List<Integer>> a2) {
        List<List<Integer>> paths = new ArrayList<>();
        for (List<Integer> item2 : a2) {
            List<Integer> path = new ArrayList<>();
            path.add(position);
            path.addAll(item2);
            paths.add(path);
        }
        return paths;
    }
}