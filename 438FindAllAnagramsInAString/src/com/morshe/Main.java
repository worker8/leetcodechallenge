package com.morshe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();
        String s = "cbaebabacd", t = "abc";
        //"cbaebabacd"
        //      ^
        //         *
        List<Integer> result = solution.findAnagrams(s, t);
        System.out.println("answer: " + result);
    }
}

class Solution {
    public List<Integer> findAnagrams(String s, String t) {
        Map<Character, Integer> map = new HashMap<>();
        List<Integer> result = new ArrayList<>();
        // 1. place target into HashMap
        for (char c : t.toCharArray()) {
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        int counter = map.size();
        // 2. use two pointers to count
        int begin = 0, end = 0;

        // 3. move end pointer upwards every loop
        while (end < s.length()) {
            char c = s.charAt(end);
            if (map.containsKey(c)) {
                map.put(c, map.get(c) - 1);
                if (map.get(c) == 0) {
                    counter--;
                }
            }
            end++;

            while (counter == 0) {
                //System.out.println("counter == 0, hashMap = " + map + "begin = " + begin + ", end = " + end);
                char beginC = s.charAt(begin);
                if (end - begin == t.length()) {
                    result.add(begin);
                }
                if (map.containsKey(beginC)) {
                    map.put(beginC, map.get(beginC) + 1);
                    if (map.get(beginC) > 0) {
                        counter++;
                    }
                }
                begin++;
            }
        }
        // 4. if contain all target strings, check if the range is correct
        //      while range not but HashMap all used up, move low begin pointer upwards

        // 5.

        return result;
    }
}