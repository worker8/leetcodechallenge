package com.morshe;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();
        List<List<Integer>> answer = solution.combine(4, 2);
        System.out.println("answer: " + answer);
    }
}

class Solution {
    List<List<Integer>> result = new ArrayList<>();
    int n, k;

    public List<List<Integer>> combine(int n, int k) {
        List<Integer> temp = new ArrayList<>();
        this.n = n;
        this.k = k;
        if (k > n) {
            return result;
        }
        if (k == 0) {
            result.add(temp);
            return result;
        }
        if (n == 0) {
            result.add(temp);
            return result;
        }
        if (n == 1 && k == 1) {
            temp.add(1);
            result.add(temp);
            return result;
        }

        f(temp, 1);
        return result;
    }

    // test-run: n = 5, k = 3
    void f(List<Integer> taken, int lower) {
        // TODO: take care of base case
        System.out.println("taken: " + taken);
        if (taken.size() == k) { // taken = []
            result.add(taken);
            return;
        }
        // normal case
        // taken = [], lower = 1   ; // 1st call
        // taken = [1], lower = 2  ; // 2nd call
        // taken = [1,2], lower = 3, i = 3; // 3nd call
        // taken = [1,2,3], lower = 4; // 4th call --> result.add(taken)
        // taken = [1,2], lower = 3; // 3nd call
        // taken = [1,2,3], lower = 4; // 4th call
        for (int i = lower; i <= n; i++) { // lower = 1
            taken.add(i);
            f(new ArrayList<>(taken), i + 1);
            taken.remove(taken.size() - 1);
        }
    }
}
