package com.morshe;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] bills = {5, 5, 5, 10, 20};
        boolean success = solution.lemonadeChange(bills);

        System.out.println("isSuccess: " + success);
    }
}

class Solution {
    int counter5 = 0;
    int counter10 = 0;
    int counter20 = 0;
    boolean failure = false;

    public boolean lemonadeChange(int[] bills) {
        for (int bill : bills) {
            boolean isSuccess = receive(bill);
            System.out.println(counter5 + "," + counter10 + "," + counter20);
            if (!isSuccess) {
                return false;
            }
        }
        return true;
    }

    private boolean receive(int bill) {
        switch (bill) {
            case 5:
                counter5++;
                return true;
            case 10:
                if (counter5 >= 1) {
                    counter10++;
                    counter5--;
                    return true;
                } else {
                    return false;
                }
            case 20:
                if (counter10 >= 1 && counter5 >= 1) {
                    counter10--;
                    counter5--;
                    counter20++;
                    return true;
                } else if (counter10 == 0 && counter5 >= 3) {
                    counter5 -= 3;
                    counter20++;
                    return true;
                } else {
                    return false;
                }
            default:
                return false;
        }
    }
}