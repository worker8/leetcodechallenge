package com.morshe;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        String temp = "hey there blue sky";
        temp.split(" ");
        ListNode node1 = new ListNode(-10);
        ListNode node2 = new ListNode(-3);
        ListNode node3 = new ListNode(0);
        ListNode node4 = new ListNode(5);
        ListNode node5 = new ListNode(9);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        Solution solution = new Solution();
        TreeNode answer = solution.sortedListToBST(node1);


        TreeNode treeNode1 = new TreeNode(0);
        TreeNode treeNode2a = new TreeNode(-3);
        TreeNode treeNode2b = new TreeNode(9);

        TreeNode treeNode3a = new TreeNode(-10);
        TreeNode treeNode3b = new TreeNode(5);

        treeNode1.left = treeNode2a;
        treeNode1.right = treeNode2b;

        treeNode2a.left = treeNode3a;
        treeNode2b.left = treeNode3b;

        printTree(answer);
    }

    static void printTree(TreeNode root) {
        // level order traversal
        Stack<TreeNode> stack = new Stack<>();
        stack.add(root);

        while (!stack.isEmpty()) {
            TreeNode currNode = stack.pop();
            System.out.println("answer: " + currNode.val);
            if (currNode.left != null) {
                stack.add(currNode.left);

            }
            if (currNode.right != null) {
                stack.add(currNode.right);
            }
        }
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }

    @Override
    public String toString() {
        return String.valueOf(val);
    }
}

class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }
}


class Solution {

    private TreeNode assign(List<TreeNode> list) {

        // odd size()
        if (list.size() == 2) {
            TreeNode first = list.get(0);
            TreeNode second = list.get(1);
            if (first.val > second.val) {
                first.left = second;
            } else {
                first.right = second;
            }
            return first;
        } else if (!list.isEmpty()) {
            int midPoint = list.size() / 2; // = 2/2 = 1
            TreeNode root = list.get(midPoint);
            TreeNode leftNode = assign(list.subList(0, midPoint)); // (0,2)
            TreeNode rightNode = assign(list.subList(midPoint + 1, list.size())); // (2,5)
            root.left = leftNode;
            root.right = rightNode;
            return root;
        } else {
            return null;
        }
    }

    TreeNode sortedListToBST(ListNode head) {
        List<TreeNode> list = new ArrayList<>();
        ListNode curr = head;
        while (curr != null) {
            list.add(new TreeNode(curr.val));
            curr = curr.next;
        }
        return assign(list);
    }
}
