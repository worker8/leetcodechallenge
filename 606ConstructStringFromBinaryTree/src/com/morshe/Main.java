package com.morshe;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
        TreeNode level1 = new TreeNode(1);
        TreeNode level2a = new TreeNode(2);
        TreeNode level2b = new TreeNode(3);
        TreeNode level3 = new TreeNode(4);
        level1.left = level2a;
        level1.right = level2b;
        level2a.left = level3;

        Solution solution = new Solution();
        solution.tree2str(level1);

    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}

class Solution {
    String tree2str(TreeNode t) {
        return f(t);
    }

    void justLearningTemp() {
        Map<Integer, ArrayList<Integer>> map = new HashMap<>();
        for (Map.Entry<Integer, ArrayList<Integer>> entry : map.entrySet()) {
            Integer key = entry.getKey();
            ArrayList<Integer> value = entry.getValue();

        }

        Set<Integer> set = map.keySet();

        Stack<Integer> stack = new Stack<>();
        Integer nextKey = map.entrySet().iterator().next().getKey();
        stack.push(nextKey);
    }

    String f(TreeNode root) {
        if (root == null) {
            return "";
        }
        String result = "";
        result = result + root.val;
        // left empty, right not
        if (root.left == null && root.right != null) {
            result += "()"; // for left that is null
            result += f(root.right);
        } else if (root.left != null && root.right == null) {
            result += "(" + f(root.left) + ")";
        } else if (root.left != null && root.right != null) {
            result += "(" + f(root.left) + ")";
            result += "(" + f(root.right) + ")";
        }
        return result;
    }


}